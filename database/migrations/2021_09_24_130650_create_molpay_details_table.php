<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMolpayDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('molpay_details', function (Blueprint $table) {
            $table->id();

            $table->string('tranID')->nullable();
            $table->string('status')->nullable();
            $table->string('amount')->nullable();
            $table->string('currency')->nullable();
            $table->string('orderid')->nullable();
            $table->integer('paid')->default(0);
            $table->string('paydate')->nullable();
            $table->string('appcode')->nullable();
            $table->string('error_code')->nullable();
            $table->string('error_desc')->nullable();
            $table->string('user_id')->nullable();
            $table->string('provider_id')->nullable();
            $table->string('request_id')->nullable();
            $table->string('order_id')->nullable();
            $table->string('type')->nullable();
            $table->longText('order_data')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('molpay_details');
    }
}
