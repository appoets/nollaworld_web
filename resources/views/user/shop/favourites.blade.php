@extends('user.layouts.app')

@section('content')
@include('user.layouts.partials.user_common')

<div class="container">
	<div class="">
		@include('user.layouts.partials.sidebar')
		<div class="tab-content mb-5">
			<div class="tab-pane container active" id="fav">
			  	<div class="container">
			  		<div class="row">
			  			<div class="col-md-12">
			  				<div class="favourites-section">
                                @forelse($available as $item)
                                    <!-- Restaurant List Box Starts -->
                                    <a href="{{url('/restaurant/details')}}?name={{$item->shop->name}}" class="food-item-box col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                        <img class="food-img bg-img" width="90%" src="{{$item->shop->avatar}}">
                                        <!-- <div class="food-img bg-img" style="background-image: url({{$item->shop->avatar}});" width="20%">
                                            <span class="heart"><i class="ion-ios-heart"></i></span>
                                        </div> -->
                                        <div class="food-details">
                                            <h6 class="food-det-tit">{{$item->shop->name}}</h6>
                                            <p class="food-det-txt">{{$item->shop->address}}</p>
                                            <p></p>
                                            <div class="food-other-details row">
                                                <div class="col-xs-3 p-r-0">
                                                    <span class="food-rating"><i class="ion-android-star"></i>{{$item->shop->rating}}</span>
                                                </div>
                                                <div class="col-xs-3 text-center">
                                                    <span class="food-deliver-time food-list-txt">30Mins</span>
                                                </div>
                                                <div class="col-xs-6 text-right">
                                                    <span class="food-quantity-price food-list-txt">$100 for two</span>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <!-- Restaurant List Box Starts -->
                                    @empty
                                    <p>No Favourite !</p>
                                @endforelse
                                <!-- Restaurant List Box Starts -->
                            </div>
                        </div>
			  			{{--<div class="fav-slider">
			  				@forelse($available as $item)
			  					<!-- <p>{{$item}}</p> -->
			  					<div class="item">
					  				<div class="favourites text-center m-2 p-4">
					  					<div class="product-img py-2">
						  					<img src="{{$item->shop->avatar}}">
						  				</div>
						  				<h4>$10.0 /each</h4>
					  					<h6>{{$item->shop->name}}</h6>
						  				<div class="fav-bottom">
						  					<a href="{{url('/restaurant/details')}}?name={{$item->shop->name}}" class="btn btn-green">Order</a>
						  				</div>
					  				</div>
					  			</div>
			  					@empty
                    			<p>No Favourite !</p>
			  				@endforelse
			  			</div>--}}
			  			{{--<div class="fav-slider">
							@forelse($available as $item)
								<p>$item</p>
					  			<div class="item">
					  				<div class="favourites text-center m-2 p-4">
						  				<div class="product-img py-2">
						  					<img src="{{$item->shop->avatar}}">
						  				</div>
					  					<h4>$10.0 /each</h4>
					  					<h6>{{$item->shop->categories[0]->name}}</h6>
					  					<p>This Dummy Text, not mean to Read</p>
						  				<div class="fav-bottom">
						  					<a href="{{url('/restaurant/details')}}?name={{$item->shop->name}}" class="btn btn-green">Order</a>
						  				</div>
					  				</div>
					  			</div>
					  	 		<!-- Restaurant List Box Starts -->
								@empty
                    			<p>No Favourite !</p>
                        	@endforelse
                        	<!-- Restaurant List Box Starts -->
					  	</div>--}}
					</div>
			  	</div>
			</div>
		</div>
	</div>
</div>
@endsection
