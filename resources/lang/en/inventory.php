<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Inventory Language Lines
    |--------------------------------------------------------------------------
    */
    'cuisine' => [
        'title' => 'Cuisines',
        'add_title' => 'Create Cuisines',
        'edit_title' => 'Edit Cuisines',
        'add_cuisine' => 'Add Cuisine',
        'sl_no' => 'Sl.No',
        'name' => 'Name',
        'action' => 'Action',
        'no_record_found' => 'No Cuisine Found'
    ],
    'category' => [
        'title' => 'Categories',
        'add_title' => 'Create Category',
        'edit_title' => 'Edit Category',
        'add_category' => 'Add Category',
        'sl_no' => 'Sl.No',
        'name' => 'Name',
        'desc' => 'Description',
        'status' => 'Status',
        'resturant_name' => 'Shop',
        'image' => 'Image',
        'action' => 'Action',
        'commision' => 'Commision',
        'no_record_found' => 'No Category Found',
        'sub_title' => 'Sub Category',
        'add_subcategory' => 'Add Sub Category',
        'addsub_title' => 'Create Sub Category',
        'editsub_title' => 'Edit Sub Category',
        'category_name' => 'Category',
        'sub_no_record_found' => 'No Sub Category Found',
        'position' => 'Category order',
        'shoptype' => 'Type'
    ],
    'shoptype' => [
        'title' => 'Shop Type',
        'add_title' => 'Create Shop Type',
        'edit_title' => 'Edit Shop Type',
        'add_shoptype' => 'Add Shop Type',
        'sl_no' => 'Sl.No',
        'name' => 'Name',
        'action' => 'Action',
        'no_record_found' => 'No Shop Type Found',
        'status' => 'Status',
    ],
    'shoptypecommision' => [
        'title' => 'Shop Type Commision',
        'add_title' => 'Create Shop Type Commision',
        'edit_title' => 'Edit Shop Type Commision',
        'add_shoptypecommision' => 'Add Shop Type Commision',
        'sl_no' => 'Sl.No',
        'shoptypename' => 'Shop Type Name',
        'shopname' => 'Shop Name',
        'commision' => 'Commision Percentage',
        'action' => 'Action',
        'no_record_found' => 'No Commision Found',
        'shop_name' => 'Shop',
        'shoptype' =>'Shop Type'
    ],
    'product' => [
        'title' => 'Products',
        'add_title' => 'Create Product',
        'edit_title' => 'Edit Product',
        'add_product' => 'Add Product',
        'sl_no' => 'Sl.No',
        'name' => 'Name',
        'desc' => 'Description',
        'status' => 'Status',
        'resturant_name' => 'Shop',
        'image' => 'Image',
        'action' => 'Action',
        'cuisine' => 'Cuisines',
        'category' => 'Category',
        'no_record_found' => 'No Product Found',
        'pricing_title' => 'Pricing',
        'price' => 'Price',
        'discount' => 'Discount',
        'discount_type' => 'Discount Type',
        'currency' => 'Currency',
        'featured' => 'Is Featured Product',
        'featured_position' => 'Featured Position',
        'featured_image' => 'Featured Image',
        'featured_image_note' => 'Note:- Please upload Image size 252x152 for featured Product',
        'addons' => 'Addons List',
        'sub_category' => 'Sub Category',
        'product_position' => 'Product Order',
        'fixed' => 'Fixed',
        'addon_fixed' => 'Fixed Addon',
        'out_of_stock' => 'Out Of Stock'
 
    ],
    'addons' => [

        'title' => 'Addons',
        'add_title' => 'Create Addon',
        'edit_title' => 'Edit Addon',
        'add_addon' => 'Add Addon',
        'sl_no' => 'Sl.No',
        'name' => 'Name',
        'image' => 'Image',
        'action' => 'Action',
        'no_record_found' => 'No Add-ons Found!',
        'created_success' => 'addons created successfully',
        'updated_success' => 'Addons Update Successfully',
        'shop' => 'Shop Name',
        'fixed' => 'Addons Required',
        'remove' => 'Addons deleted Successfully.',
        'not_remove'=> 'you can`t remove this addon since it is in use.'

    ],
    'cart' => [
        'added' => 'Added Successfully'
    ]
       

    
      

];
