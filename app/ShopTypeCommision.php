<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopTypeCommision extends Model
{
    use SoftDeletes;

    protected $table = 'shop_type_commision';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shop_id',
        'shop_type_id',
        'commision'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
    ];

    /**
     * ShopType belonging to the ShopTypeCommision
     */
    public function shoptype()
    {
        return $this->belongsTo('App\ShopType','shop_type_id');
    }

    /**
     * Shop belonging to the ShopTypeCommision
     */
    public function shop()
    {
        return $this->belongsTo('App\Shop','shop_id');
    }
}
