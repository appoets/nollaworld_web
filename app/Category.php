<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Category extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'parent_id',
        'shop_id',
        'position',
        'commision',
        'is_admin',
        'status',
        'shop_type_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
    ];
    //protected $with = ['subcategories'];

    /**
     * Category Images
     */
    public function images()
    {
        return $this->hasMany('App\CategoryImage');
    }

    /**
     * Category 
     */
    public function subcategories()
    {
        return $this->hasMany('App\Category', 'parent_id', 'id');
    }

    /**
     * Category 
     */
    public function categories()
    {
        return $this->hasMany('App\Category', 'id');
    }

    /**
     * SubCategory 
     */
    public function subcategory()
    {
        return $this->hasMany('App\SubCategory');
    }

    /**
     * Categories that the category belongs to.
     */
    public function categoryproducts()
    {
        return $this->hasMany('App\CategoryProduct');
    }

    /**
     * Products belonging to the category
     */
    public function products()
    {
        return $this->belongsToMany('App\Product')->where('status','enabled')->orderBy(\DB::raw('ISNULL(position), position'),'ASC');;
    }

    /**
     * Shoptype that the Categories belongs to.
     */
    public function shoptype()
    {
        return $this->belongsTo('App\ShopType','shop_type_id');
    }

    /**
     * Get the list of all categories along with subcategories and images.
     */
    public function scopeList($query, $shop_id = NULL,$user_id = NULL)
    {
        return $query->with(['products','products.prices','products.images','products.addons','products.cart' => function($query) use ($user_id){
                return $query->where('user_id', $user_id);
            },'products.cart.cart_addons','images'])->where('shop_id',$shop_id)->get();
    }

    public function scopeSearch($query, $shop_id = NULL,$user_id = NULL,$category_id= NULL, $prodname= NULL)
    {
        return $query->with(['products' => function($query) use ($prodname){ 
            return $query->where('name', 'LIKE', '%' . $prodname . '%');},'products.prices','products.images','products.addons','products.cart' => function($query) use ($user_id){
                return $query->where('user_id', $user_id);
            },'products.cart.cart_addons','images'])->where('shop_id',$shop_id)->where('id',$category_id)->get();
    }

    public function scopeFilterbybrand($query, $shop_id = NULL,$user_id = NULL,$category_id= NULL, $prodbrand= NULL)
    {
        return $query->with(['products' => function($query) use ($prodname){ 
            return $query->whereIn('brand',$prodname);},'products.prices','products.images','products.addons','products.cart' => function($query) use ($user_id){
                return $query->where('user_id', $user_id);
            },'products.cart.cart_addons','images'])
             ->where('shop_id',$shop_id)->where('id',$category_id)
            ->get();
    }

    /**
     * Get the list of all categories along with subcategories and images.
     */
    public function scopeListwithsubcategory($query, $shop_id = NULL,$user_id = NULL)
    {
        return $query->with(['subcategories.products','subcategories.products.prices','subcategories.products.images','subcategories.products.addons','subcategories.products.cart' => function($query) use ($user_id){
                return $query->where('user_id', $user_id);
            }])->where('shop_id',$shop_id)->get();
    }

    /**
     * Get the list of all products that belong to the category with prices and images.
     */
    public function scopeProductList($query, $shop_id = NULL)
    {
        return $query->where('shop_id',$shop_id)->with('products', 'products.images', 'products.prices', 'products.variants', 'products.variants.images', 'products.variants.prices','images');
    }

    public function scopeListwithshoptype($query)
    {
        return $query->with(['shoptype.name'])->get();
    }
}