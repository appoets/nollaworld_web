<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopDeliveryOption extends Model
{
   protected $fillable = [
        
        'shop_id',
        'name',
        'delivery_option_id'
        ];
}