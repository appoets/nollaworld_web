@extends('user.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default mt-100">
                <!-- <div class="panel-heading">@lang('user.register')</div> -->
                <h3 class="font-weight-60 text-12 mb-4">@lang('user.register')</h3>
                @include('include.alerts')
                <div class="panel-body">
                <div class="print-error-msg">
                    <ul>
                    </ul>
                </div>
                 <form role="form" method="POST" id="register_form" action="{{ url('/register') }}">
            
                        <div id="first_step">
                            <div class="form-group row">
                            <div class="col-md-3">
                                <div class="easy-autocomplete eac-round" style="width: 0px;">
                                    <input type="text" id="country_code" name="country_code" class="form-control country_code" placeholder="+1" autocomplete="off">
                                    <!-- <input value="+91" type="text" placeholder="+1" id="country_code" name="country_code" /> -->
                                    <div class="easy-autocomplete-container" id="eac-container-login_code">
                                        <ul></ul>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-9 p-l-0">

                                 <input type="text" autofocus id="phone_number" class="form-control" placeholder="@lang('enter_phone')" name="phone_number" value="{{ old('phone_number') }}" />
                            </div>
                            </div>

                            <!-- <div class="col-md-4">
                                <input value="+91" type="text" placeholder="+1" id="country_code" name="country_code" />
                            </div> 
                            
                            <div class="col-md-8">
                                <input type="text" autofocus id="phone_number" class="form-control" placeholder="@lang('enter_phone')" name="phone_number" value="{{ old('phone_number') }}" />
                            </div> -->

                            <div class="col-md-8">
                                @if ($errors->has('phone_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                @endif
                            </div>

                             <div class="col-md-12 mobile_otp_verfication" style="display: none;">
                                <input type="text" class="form-control" placeholder="@lang('user.otp')" name="otp" id="otp" value="">

                                @if ($errors->has('otp'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('otp') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <input type="hidden" id="otp_ref"  name="otp_ref" value="" />
                            <input type="hidden" id="otp_phone"  name="phone" value="" />

                            <div class="form-group" id="mobile_verfication">
                        <button class="btn btn-dark btn-lg btn-block my-4" type="button" onclick="smsLogin();" value="Verify Phone Number"> Verify Phone Number</button>
                    </div>
                            <!-- <div class="col-md-12" style="padding-bottom: 10px;" id="mobile_verfication">
                                <input type="button" class="log-teal-btn small" onclick="smsLogin();" value="Verify Phone Number"/>
                            </div> -->

                             <!-- <div class="col-md-12 mobile_otp_verfication" style="padding-bottom: 10px;display:none" id="mobile_otp_verfication">
                                <input type="button" class="log-teal-btn small" onclick="checkotp();" value="Verify Otp"/>
                            </div> -->
                              <div class="form-group mobile_otp_verfication" id="mobile_otp_verfication" style="padding-bottom: 10px;display:none">
                        <button class="btn btn-dark btn-lg btn-block my-4" type="button" onclick="checkotp();" value="Verify Otp"> Verify Otp</button>
                    </div>



                        </div>

                        {{ csrf_field() }}

                        <div id="second_step" style="display: none;">

                            <div class="col-md-12">
                                <input type="text" class="form-control" placeholder="@lang('user.create.first_name')" name="first_name" value="{{ old('first_name') }}">

                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-12">
                                <input type="text" class="form-control" placeholder="@lang('user.create.last_name')" name="last_name" value="{{ old('last_name') }}">

                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-12">
                                <input type="email" class="form-control" name="email" placeholder="@lang('user.create.email')" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif                        
                            </div>


                            
                            <div class="col-md-12">
                                <input type="password" class="form-control" name="password" placeholder="@lang('user.create.password')">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-12">
                                <input type="password" placeholder="@lang('user.create.confirm_password')" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                            
                            <!-- <div class="col-md-12">
                                <button class="log-teal-btn register_btn" type="submit">@lang('user.register')</button>
                            </div> -->
                               <div class="form-group">
                        <button class="btn btn-dark btn-lg btn-block my-4" type="submit"> @lang('user.register')</button>
                    </div>

                        </div>

                    </form> 
                    
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript">
    var my_otp='';
    function smsLogin(){
        var countryCode = document.getElementById("country_code").value;
        var phoneNumber = document.getElementById("phone_number").value;
        $('#otp_phone').val(countryCode+''+phoneNumber);
        var csrf = $("input[name='_token']").val();;

            $.ajax({
                url: "{{url('/otp')}}",
                type:'POST',
                data:{ phone : countryCode+''+phoneNumber,'_token':csrf },
                success: function(data) { 
                    if($.isEmptyObject(data.error)){
                        my_otp=data.otp;
                        $('.mobile_otp_verfication').show();
                        $('#mobile_verfication').hide();
                        $('#mobile_verfication').html("<p class='helper'> Please Wait... </p>");
                        $('#phone_number').attr('readonly',true);
                        $('#country_code').attr('readonly',true);
                        $(".print-error-msg").find("ul").html('');
                        $(".print-error-msg").find("ul").append('<li>'+data.message+'</li>');
                    }else{
                        printErrorMsg(data.error);
                    }
                },
                error:function(jqXhr,status) { 
                    if(jqXhr.status === 422) {
                        $(".print-error-msg").show();
                        var errors = jqXhr.responseJSON;

                        $.each( errors , function( key, value ) { 
                            $(".print-error-msg").find("ul").append('<li>'+value[0]+'</li>');
                        }); 
                    } 
                }

                });
    }
    function checkotp(){
        var otp = document.getElementById("otp").value;
        if(otp){
            if(my_otp == otp){
                $(".print-error-msg").find("ul").html('');
                $('#mobile_otp_verfication').html("<p class='helper'> Please Wait... </p>");
                $('#phone_number').attr('readonly',true);
                $('#country_code').attr('readonly',true);
                $('.mobile_otp_verfication').hide();
                $('#second_step').fadeIn(400);
                $('#mobile_verfication').show().html("<p class='helper'> * Phone Number Verified </p>");
                my_otp='';
            }else{
                $(".print-error-msg").find("ul").html('');
                $(".print-error-msg").find("ul").append('<li>Otp not Matched!</li>');
            }
        }
    }
    function printErrorMsg (msg) { 
        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display','block');
        $(".print-error-msg").show();
        $.each( msg, function( key, value ) {
            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
        });
    }
</script>
<script type="text/javascript">
    let digitValidate = function(ele){
  console.log(ele.value);
  ele.value = ele.value.replace(/[^0-9]/g,'');
}

let tabChange = function(val){
    let ele = document.querySelectorAll('input');
    if(ele[val-1].value != ''){
      ele[val].focus()
    }else if(ele[val-1].value == ''){
      ele[val-2].focus()
    }   
 }


</script>
@endsection

