<?php

return array(

    'IOSUser'     => array(
        'environment' =>env('IOS_USER_ENV', 'development'),
        'certificate' => app_path().'/apns/user/Instauser.pem',
        'passPhrase'  => env('IOS_PUSH_PASS', 'Appoets123$'),
        'service'     =>'apns'
    ),
    'IOSProvider' => array(
        'environment' => env('IOS_PROVIDER_ENV', 'development'),
        'certificate' => app_path().'/apns/provider/instacartDelivery.pem',
        'passPhrase'  => env('IOS_PROVIDER_PUSH_PASS', 'Appoets123$'),
        'service'     => 'apns'
    ),
    'IOSShop' => array(
        'environment' => env('IOS_SHOP_ENV', 'development'),
        'certificate' => app_path().'/apns/shop/Certificates.pem',
        'passPhrase'  => env('IOS_SHOP_PUSH_PASS', 'Appoets123$'),
        'service'     => 'apns'
    ),
    'AndroidUser' => array(
        'environment' =>env('ANDROID_ENV', 'development'),
        'apiKey'      =>env('ANDROID_PUSH_KEY', 'yourAPIKey'),
        'service'     =>'gcm'
    )

);
