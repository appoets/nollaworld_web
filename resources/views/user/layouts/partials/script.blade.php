<script>
    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
</script>



@if(Auth::guest())
<!-- SignIn Modal 1-->
<div class="modal" id="signin1" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-zoom modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <!-- Step1-->
                <div class="signin-step1">
                    <h4 class="form-header text-center py-2">Welcome!</h4>
                    <div class="signin-form">
                        <form action="{{ url('/login') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group row">
                            <div class="col-md-3">
                                <input type="text" id="login_code" name="login_code" class="form-control country_code" placeholder="+1">

                            </div>
                            <div class="col-md-9 p-l-0">

                                <input type="text" class="form-control" id="phone" name="phone"
                                    aria-describedby="emailHelp" placeholder="Phone Number">
                            </div>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" id="password"
                                    id="exampleInputPassword1" placeholder="Password">
                            </div>
                            <p class="text-center btn-login-next1">Forgot your Password? <a href="#">Recover Here</a>
                            </p>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-green btn-login mx-auto">Submit</button>
                            </div>
                        </form>

                        <p class="text-center">Don't Have an Account? <a href="#" class="signup_link">Signup Here</a></p>
                    </div>
                </div>

                <!-- Step2-->
                <div class="signin-step2 text-center">
                    <h4 class="form-header text-center py-2">Forgot Password</h4>
                    <img src="assets/images/login/key.png" class="py-2 text-center img-fluid" width="15%">
                    <div class="py-2">
                        <h6><b>Please Enter Your Verificatio Code</b></h6>
                        <span>We Have sent an Verification Code for your Registered Email-ID</span>
                    </div>
                    <div class="signin-form">
                        <div class="row">
                            <div class="form-group">
                                <input type="text" class="form-control verification-box" id=""
                                    aria-describedby="emailHelp" placeholder="">
                                <input type="text" class="form-control verification-box" id=""
                                    aria-describedby="emailHelp" placeholder="">
                                <input type="text" class="form-control verification-box" id=""
                                    aria-describedby="emailHelp" placeholder="">
                                <input type="text" class="form-control verification-box" id=""
                                    aria-describedby="emailHelp" placeholder="">
                            </div>
                        </div>

                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-green btn-login mx-auto btn-login-next2">Next <i
                                    class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>

                <!-- Step3-->
                <div class="signin-step3">
                    <h4 class="form-header text-center py-2">Forgot Password</h4>
                    <div class="text-center">
                        <img src="assets/images/login/password.png" class="py-2 text-center img-fluid" width="15%">
                    </div>
                    <div class="text-center py-2">
                        <h6><b>Please Enter a New Password</b></h6>
                    </div>
                    <div class="signin-form">
                        <div class="form-group">
                            <input type="password" class="form-control" id="" aria-describedby="emailHelp"
                                placeholder=" New Password">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="" aria-describedby="emailHelp"
                                placeholder="Re-enter Password">
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-green btn-login mx-auto">Change Password</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Sign up Modal-->

<!-- SignUp Modal 2-->
<div class="modal" id="signup" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-zoom modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="signup-step1">
                    <div class="text-center py-2">
                        <h4 class="form-header text-center py-2">Create Account</h4>
                    </div>
                    <form method="POST" action="{{ url('/register') }}" enctype="multipart/form-data">

                    {{ csrf_field() }}
                    <div class="avatar-upload">
                        <div class="avatar-edit">
                            <input type='file' id="imageUpload" name="avatar" accept=".png, .jpg, .jpeg" />
                            <label for="imageUpload"></label>
                        </div>
                        <div class="avatar-preview">
                            <div id="imagePreview" style="background-image: url(assets/images/avatar.png);">
                            </div>
                        </div>
                    </div>
                    <div class="signin-form">
                      
                            <div class="form-group">
                                <input type="text" class="form-control" name="first_name" id="first_name"
                                    aria-describedby="emailHelp" value="{{ old('first_name') }}"
                                    placeholder=" First Name">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="last_name" name="last_name"
                                    aria-describedby="emailHelp" value="{{ old('last_name') }}" placeholder="Last Name">
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" id="next1"
                                    class="btn btn-green btn-login btn-reg-next1 mx-auto">Next <i
                                        class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                            </div>
                    </div>
                </div>

                <!---step 2-->
                <div class="signup-step2">
                    <div class="text-center py-2">
                        <h4 class="form-header text-center py-2">Your Mail Id</h4>
                    </div>
                    <div class="text-center">
                        <img src="assets/images/login/mail.png" class="py-2 text-center img-fluid" width="15%">
                    </div>
                    <div class="signin-form">
                        <div class="form-group">
                            <input type="email" class="form-control" id="email" name="email"
                                aria-describedby="emailHelp" placeholder=" Email" required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="password" name="password"
                                aria-describedby="emailHelp" placeholder="Password" required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="password_confirmation"
                                name="password_confirmation" aria-describedby="emailHelp" placeholder="Confirm Password"
                                required>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-green btn-login btn-reg-next2 mx-auto">Next <i
                                    class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>

                <!---step 3-->
                <div class="signup-step3">
                    <div class="text-center py-2">
                        <h4 class="form-header text-center py-2">Your Phone Number</h4>
                    </div>
                    <div class="text-center">
                        <img src="assets/images/login/phone.png" class="py-2 text-center img-fluid" width="15%">
                    </div>
                    <div class="signin-form">

                        <div class="form-group row">
                                                      
                            <div class="col-md-3">
                                <input type="text" id="country_code" name="country_code" class="country_code form-control" placeholder="+1">
                            </div>
                            <div class="col-md-9 p-l-0">
                                <input type="number" min="0" class="form-control phone-number" id="phone_number" name="phone_number" value="{{ old('phone_number') }}" placeholder="@lang('user.enter_phone')" maxlength="10" onkeypress="return isNumberKey(event);" required>
                            </div> 
                                                
                        </div>


                        <div class="form-group text-center">
                            <button type="submit" onclick="smsLogin();"
                                class="btn btn-green btn-login btn-reg-next3 mx-auto">Next <i
                                    class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>

                <!-- Step4-->
                <div class="signup-step4 text-center">
                    <h4 class="form-header text-center py-2">Forgot Password</h4>
                    <img src="assets/images/login/key.png" class="py-2 text-center img-fluid" width="15%">
                    <div class="py-2">
                        <h6><b>Please Enter Your Verification Code</b></h6>
                        <span>Enter OTP sent to Mobile Number</span>
                    </div>
                    <div class="signin-form">
                        <div class="row">
                            <div class="form-group">
                                <!-- <input type="text" class="form-control " placeholder="@lang('user.enter_phone')" name="otp" id="otp" value="" > -->

                                <input type="text" class="form-control verification-box" id="otp" name=otp maxlength="6"
                                    aria-describedby="emailHelp" placeholder="">

                            </div>
                        </div>

                        <input type="hidden" id="otp_ref" name="otp_ref" value="" />
                        <input type="hidden" id="otp_phone" name="phone" value="" />

                        <p class="py-2 text-center"><a href="#" onclick="smsLogin();">Resend OTP</a></p>
                        <div class="form-group text-center">
                            <button type="button" class="btn btn-green btn-login mx-auto  mobile_otp_verfication"
                                onclick="checkotp();" value="Verify Otp" style="display: none;">Verify OTP</button>

                            <!-- <button type="submit" onclick="checkotp();" class="btn btn-green btn-login mx-auto ">Verify <i class="fa fa-long-arrow-right" aria-hidden="true"></i></button> -->
                        </div>
                        <div class="form-group text-center">
                            <!-- <button type="button" class="login-btn register_btn">@lang('user.footer.sign_up')</button> -->

                            <button type="submit" class="btn btn-green btn-login mx-auto ">Submit <i
                                    class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                        </div>
                    </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- End Sign up Modal-->

@endif
<style>
	.pac-container {
		z-index: 9999999999999999999 !important;
	}
</style>
<script src="{{ asset('assets/user/js/jquery.easy-autocomplete.js') }}" type="text/javascript"></script>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/user/css/easy-autocomplete.min.css')}}">
 <style type="text/css">
        .easy-autocomplete-container ul { max-height: 200px !important; overflow: auto !important; }
        .easy-autocomplete { width:200px!important; }
        .phone_fileds {
            margin-left: 0px !important;
            border-left: 1px solid #ccc !important;
            width: 100% !important
        }
        .no-pad{
            padding: 0px !important;
        }
    </style>
 <script>
function phoneNumberKey(evt)
{
    console.log('hi');
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 
    && (charCode < 48 || charCode > 57))
        return false;

    return true;
}    
function isNumberKey(evt)
{
    var edValue = document.getElementById("phone_number");
    var s = edValue.value;
    if (event.keyCode == 13) {
        event.preventDefault();
        if(s.length>=10){
            smsLogin();
        }
    }
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 
    && (charCode < 48 || charCode > 57))
        return false;

    return true;
}    
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
var options = {

  url: "{{asset('assets/user/js/countryCodes.json')}}",

  getValue: "dial_code",

  list: {
    match: {
      enabled: true
    },
    onClickEvent: function() {
            var value = $(".country_code").getSelectedItemData().dial_code;

            $(this).val(value).trigger("change");
        },
    maxNumberOfElements: 1000
  },
  minCharNumber: 1,
  template: {
    type: "custom",
    method: function(value, item) {
      return "<span class='flag flag-" + (item.dial_code).toLowerCase() + "' ></span>" +value+" ( "+item.name+" ) ";
    }
  },

  theme: "round"
};
$(".country_code").easyAutocomplete(options);
</script>
 
<script type="text/javascript">
    function googleTranslateElementInit() {
        new google.translate.TranslateElement({
            pageLanguage: 'en',
            layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT
        }, 'google_translate_element');
    }

    function triggerHtmlEvent(element, eventName) {
        var event;
        if (document.createEvent) {
            event = document.createEvent('HTMLEvents');
            event.initEvent(eventName, true, true);
            element.dispatchEvent(event);
        } else {
            event = document.createEventObject();
            event.eventType = eventName;
            element.fireEvent('on' + event.eventType, event);
        }
    }

    jQuery('.lang-select').click(function () {
        var theLang = jQuery(this).attr('data-lang');
        jQuery('.goog-te-combo').val(theLang);

        //alert(jQuery(this).attr('href'));
        window.location = jQuery(this).attr('href');
        location.reload();

    });
</script>


<script type="text/javascript">
    function checkotp() {

        var otp = document.getElementById("otp").value;
        var my_otp = $('#otp_ref').val();
        if (otp) {
            if (my_otp == otp) {
                $(".print-error-msg").find("ul").html('');
                $('#mobile_otp_verfication').html("<p class='helper'> Please Wait... </p>");
                $('#phone_number').attr('readonly', true);
                $('#country_code').attr('readonly', true);
                $('.mobile_otp_verfication').hide();
                $('#second_step').fadeIn(400);
                $('#mobile_verfication').show().html("<p class='helper'> * Phone Number Verified </p>");
                my_otp = '';
            } else {
                $(".print-error-msg").find("ul").html('');
                $(".print-error-msg").find("ul").append('<li>Otp not Matched!</li>');
            }
        }
    }



    function smsLogin() {

        // $('.exist-msg').hide();
        var countryCode = document.getElementById("country_code").value;
        var phoneNumber = document.getElementById("phone_number").value;
        $('#otp_phone').val(countryCode + '' + phoneNumber);
        var csrf = $("input[name='_token']").val();;
        var  phone = countryCode+''+phoneNumber;

        $.ajax({
            url: "{{url('/otp')}}",
            type: 'POST',
            data: {
                phone: phone,
                '_token': csrf,
                phoneonly: phoneNumber
            },
            success: function (data) {

                if ($.isEmptyObject(data.error)) {
                    $('#otp_ref').val(data.otp);
                    $('.mobile_otp_verfication').show();
                    $('#mobile_verfication').hide();
                    $('#mobile_verfication').html("<p class='helper'> Please Wait... </p>");
                    $('#phone_number').attr('readonly', true);
                    $('#country_code').attr('readonly', true);
                    $(".print-error-msg").find("ul").html('');
                    $(".print-error-msg").find("ul").append('<li>' + data.message + '</li>');
                } else {

                    printErrorMsg(data.error);
                }
            },
            error: function (jqXhr, status) {
                if (jqXhr.status === 422) {
                    $(".print-error-msg").show();
                    var errors = jqXhr.responseJSON;

                    $.each(errors, function (key, value) {
                        $(".print-error-msg").find("ul").append('<li>' + value + '</li>');
                    });
                }
            }

        });
    }

    function printErrorMsg(msg) {

        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display', 'block');

        $(".print-error-msg").show();

        $(".print-error-msg").find("ul").append('<li><p>' + msg + '</p></li>');

    }




    $(document).ready(function () {
        $('.customer-logos').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1500,
            arrows: false,
            dots: false,
            pauseOnHover: false,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            }, {
                breakpoint: 520,
                settings: {
                    slidesToShow: 2
                }
            }]
        });
    });
</script>

<script type="text/javascript">
    //login page
    $(document).ready(function () {
        $(".signin-step2, .signin-step3").hide();
        $(".btn-login-next1").click(function () {
            event.preventDefault()
            $(".signin-step2").show();
            $(".signin-step1, .signin-step3").hide();
        });
        $(".btn-login-next2").click(function () {
            event.preventDefault()
            $(".signin-step3").show();
            $(".signin-step1, .signin-step2").hide();
        });
    });

    $(document).ready(function () {
        // $("p a.signup_link").click(function () {
        // alert("yes");
        // $(".signup-step1").show();
        // });
         $('body').on('click', 'a.signup_link', function() {
            $("#signin1").hide()
            $("#signup").show();
        });
    });


    //register page
    $(document).ready(function () {
        $(".signup-step2, .signup-step3, .signup-step4").hide();
        $(".btn-reg-next1").click(function () {
            event.preventDefault()
            $(".signup-step2").show();
            $(".signup-step1, .signup-step3, .signup-step4").hide();
        });
        $(".btn-reg-next2").click(function () {
            event.preventDefault()
            $(".signup-step3").show();
            $(".signup-step1, .signup-step2, .signup-step4").hide();
        });
        $(".btn-reg-next3").click(function () {
            event.preventDefault()
            $(".signup-step4").show();
            $(".signup-step1, .signup-step2, .signup-step3").hide();
        });
    });
</script>

<!-- image Upload -->
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                $('#imagePreview').hide();
                $('#imagePreview').fadeIn(650);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#imageUpload").change(function () {
        readURL(this);
    });
</script>

<!-- add card page -->
<script type="text/javascript">
    $(document).ready(function () {
        $(".card-block-2").hide();
        $(".card-next-1").click(function () {
            event.preventDefault()
            $(".card-block-2").show();
            $(".card-block-1").hide();
        });
    });
</script>

<!-- add address page -->
<script type="text/javascript">
    $(document).ready(function () {
        $(".address-form").hide();
        $(".add-address").click(function () {
            event.preventDefault()
            $(".address-form").show();
            $(".address-box, .add-address-row").hide();
        });
    });
</script>

<!-- View Orders page -->
<!-- <script type="text/javascript">
    $(document).ready(function () {
        $(".order-details-box").hide();
        $(".view-details").click(function () {
            event.preventDefault()
            $(".order-details-box").show();
            $(".store-list").hide();
        });
    });
</script> -->

<script type="text/javascript">
    function increaseValue() {
        var value = parseInt(document.getElementById('number').value, 10);
        value = isNaN(value) ? 0 : value;
        value++;
        document.getElementById('number').value = value;
    }

    function decreaseValue() {
        var value = parseInt(document.getElementById('number').value, 10);
        value = isNaN(value) ? 0 : value;
        value < 1 ? value = 1 : '';
        value--;
        document.getElementById('number').value = value;
    }

    function increaseValue1() {
        var value = parseInt(document.getElementById('number1').value, 10);
        value = isNaN(value) ? 0 : value;
        value++;
        document.getElementById('number1').value = value;
    }

    function decreaseValue1() {
        var value = parseInt(document.getElementById('number1').value, 10);
        value = isNaN(value) ? 0 : value;
        value < 1 ? value = 1 : '';
        value--;
        document.getElementById('number1').value = value;
    }
</script>

<!-- 
@if(Request::segment(1)=='restaurant' && Request::has('myaddress'))
<div class="modal" id="add_address" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-zoom modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body">

                <div class="row address-form">
                    <div class="col-md-6">
                        <form action="{{route('useraddress.store')}}" method="POST" id="comon-form" class="common-form">
                            {{ csrf_field() }}
                            <div class="input-section">
                                <div class="form-group">
                                    <label>Address</label>
                                    <input class="form-control addr-mapaddrs" id="pac-input" name="map_address"
                                        type="text" value="{{Session::get('search_loc')}}"
                                        placeholder="enter your address">
                                    <input type="hidden" id="latitude" name="latitude"
                                        value="{{ Session::get('latitude') }}" readonly required>
                                    <input type="hidden" id="longitude" name="longitude"
                                        value="{{ Session::get('longitude') }}" readonly required>
                                </div>
                                <div class="form-group">
                                    <label>Door / Flat no.</label>
                                    <input class="form-control addr-building" name="building" type="text" value="">
                                </div>
                                <div class="form-group">
                                    <label>Landmark</label>
                                    <input class="form-control addr-landmark" name="landmark" type="text">
                                </div>
                                <div class="form-group">
                                    <label>Address Type</label>
                                    <select class="form-control addr-type" name="type">
                                        @foreach($add_type as $key => $item)
                                        <option value="{{$key}}">{{$item}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <button class="btn btn-green float-right">Save &amp; Proceed</button>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <div class="" id="my_map" style="width: 100%; height: 200px;"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif --->


@if(Request::segment(1)=='restaurant' && Request::has('myaddress'))
 <div class="modal" id="add_address" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-zoom modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <!-- Step1-->
                <div class="signin-step1">
                    <h4 class="form-header text-center py-2">Add Address</h4>
                    <form action="{{route('useraddress.store')}}" method="POST" id="comon-form" class="common-form">
                            {{ csrf_field() }}     
                            <div class="" id="my_map" style="width: 100%; height: 200px;"></div>
                   
                        <div class="form-group">
                        <label>Address</label>
                                    <input class="form-control addr-mapaddrs" id="pac-input" name="map_address"
                                        type="text" value="{{Session::get('search_loc')}}"
                                        placeholder="enter your address">
                                    <input type="hidden" id="latitude" name="latitude"
                                        value="{{ Session::get('latitude') }}" readonly required>
                                    <input type="hidden" id="longitude" name="longitude"
                                        value="{{ Session::get('longitude') }}" readonly required>
                            <!-- <input type="text" class="form-control" placeholder="Address"> -->
                        </div>
                        <div class="form-group">
                        <label>Door / Flat no.</label>
                                    <input class="form-control addr-building" name="building" type="text" value="">
                            <!-- <input type="text" class="form-control" placeholder="Zip Code"> -->
                        </div>
                        <div class="form-group">
                            <!-- <input type="text" class="form-control" placeholder="Landmark"> -->
                            <label>Landmark</label>
                                    <input class="form-control addr-landmark" name="landmark" type="text">
                        </div>
                        <div class="form-group">
                            <!-- <select class="form-control" id="" placeholder="Add Address Type">
                                <option selected>Home</option>
                                <option>Work</option>
                            </select> -->
                            <label>Address Type</label>
                                    <select class="form-control addr-type" name="type">
                                        @foreach($add_type as $key => $item)
                                        <option value="{{$key}}">{{$item}}</option>
                                        @endforeach
                                    </select>
                        </div>
                        <!-- <a class="btn btn-green float-right" href="#">Save Address </a> -->
                        <button class="btn btn-green float-right">Save &amp; Proceed</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@if(Request::segment(1)=='' || Request::segment(1)=='restaurants' || Request::segment(1)=='useraddress' ||
Request::segment(1)=='orders' || Request::get('myaddress') || Request::segment(1)=='restaurant')
<script>
    var map;
    var input = document.getElementById('pac-input');
    var latitude = document.getElementById('latitude');
    var longitude = document.getElementById('longitude');
    var input_cur = document.getElementById('pac-input_cur');
    var latitude_cur = document.getElementById('latitude_cur');
    var longitude_cur = document.getElementById('longitude_cur');
    var address = document.getElementById('address');

    function initMap() {

        var userLocation = new google.maps.LatLng(
            13.066239,
            80.274816
        );

        map = new google.maps.Map(document.getElementById('my_map'), {
            center: userLocation,
            zoom: 15
        });

        var service = new google.maps.places.PlacesService(map);
        var autocomplete = new google.maps.places.Autocomplete(input);
        var infowindow = new google.maps.InfoWindow();

        autocomplete.bindTo('bounds', map);
        autocomplete.setFields(['geocode']);

        var infowindow = new google.maps.InfoWindow({
            content: "Shop Location",
        });

        var marker = new google.maps.Marker({
            map: map,
            draggable: true,
            anchorPoint: new google.maps.Point(0, -29)
        });

        marker.setVisible(true);
        marker.setPosition(userLocation);
        infowindow.open(map, marker);

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (location) {
                console.log(location);
                var userLocation = new google.maps.LatLng(
                    location.coords.latitude,
                    location.coords.longitude
                );

                latitude_cur.value = location.coords.latitude;
                longitude_cur.value = location.coords.longitude;


                //var latLngvar = location.coords.latitude+' '+location.coords.longitude+"   ";
                var latlng = {
                    lat: parseFloat(location.coords.latitude),
                    lng: parseFloat(location.coords.longitude)
                };
                getcustomaddress(latlng);
                marker.setPosition(userLocation);
                map.setCenter(userLocation);
                map.setZoom(13);
            });
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
        }

        google.maps.event.addListener(map, 'click', updateMarker);
        google.maps.event.addListener(marker, 'dragend', updateMarker);

        function getcustomaddress(latLngvar) {
            var geocoder = new google.maps.Geocoder();
            console.log(latLngvar);
            geocoder.geocode({
                'latLng': latLngvar
            }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    //console.log(results[0]);
                    if (results[0]) {

                        input_cur.value = results[0].formatted_address;

                        //updateForm(event.latLng.lat(), event.latLng.lng(), results[0].formatted_address);
                    } else {
                        alert('No Address Found');
                    }
                } else {
                    alert('Geocoder failed due to: ' + status);
                }
            });
        }

        function updateMarker(event) {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                'latLng': event.latLng
            }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        input.value = results[0].formatted_address;
                        updateForm(event.latLng.lat(), event.latLng.lng(), results[0].formatted_address);
                    } else {
                        alert('No Address Found');
                    }
                } else {
                    alert('Geocoder failed due to: ' + status);
                }
            });

            marker.setPosition(event.latLng);
            map.setCenter(event.latLng);
        }

        autocomplete.addListener('place_changed', function (event) {
            marker.setVisible(false);
            var place = autocomplete.getPlace();

            if (place.hasOwnProperty('place_id')) {
                if (!place.geometry) {
                    window.alert("Autocomplete's returned place contains no geometry");
                    return;
                }
                updateLocation(place.geometry.location);
            } else {
                service.textSearch({
                    query: place.name
                }, function (results, status) {
                    if (status == google.maps.places.PlacesServiceStatus.OK) {
                        updateLocation(results[0].geometry.location, results[0].formatted_address);
                        input.value = results[0].formatted_address;

                    }
                });
            }
        });

        function updateLocation(location) {
            map.setCenter(location);
            marker.setPosition(location);
            marker.setVisible(true);
            infowindow.open(map, marker);
            updateForm(location.lat(), location.lng(), input.value);
        }

        function updateForm(lat, lng, addr) {
            console.log(lat, lng, addr);
            latitude.value = lat;
            longitude.value = lng;
            @if(Request::get('search_loc'))
            $('#my_map_form').submit();
            @endif
        }
    }
    $('.my_map_form_current').on('click', function () {
        $('#my_map_form_current').submit();
    })

    /*$('.pac-input').on('blur',function(){
        if($('#latitude').val()!=''){
            $('#my_map_form').submit();
        }
    })*/
</script>



@endif
<script
    src="https://maps.googleapis.com/maps/api/js?key={{Setting::get('GOOGLE_API_KEY')}}&libraries=places&callback=initMap"
    async defer>
</script>
