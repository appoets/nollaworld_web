<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MolpayDetail extends Model
{
    protected $fillable = [
        'tranID',
        'status',
        'amount',
        'currency',
        'paydate',
        'orderid',
        'order_id',
        'type',
        'appcode',
        'error_code',
        'error_desc',
        'user_id',
        'order_data',
        'provider_id',
        'request_id'
    ];


    protected $hidden = [
        'created_at', 'updated_at'
    ];

}
