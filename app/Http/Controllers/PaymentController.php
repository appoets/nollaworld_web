<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\SendPushNotification;

use Stripe\Charge;
use Stripe\Stripe;
use Stripe\StripeInvalidRequestError;

use Auth;
use Setting;
use Exception;

use App\Http\Controllers\UserResource\OrderResource;

use App\Card;
use App\MolpayDetail;
use App\User;
use App\WalletPassbook;
use App\Order;
use App\OrderInvoice;
use Braintree_Customer;
use Braintree_ClientToken;
use Braintree_Transaction ;
use Braintree_PaymentMethodNonce;
use Braintree_Exception_NotFound;
use Braintree_PaymentMethod;

class PaymentController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

       /**
     * payment for user.
     *
     * @return \Illuminate\Http\Response
     */
    public function payment(Request $request)
    {
        /*$this->validate($request, [
                'order_id' => 'required|exists:order_invoices,id,paid,0|exists:orders,id,user_id,'.Auth::user()->id
            ]);*/

        if($request->payment_mode == 'RAZER'){
            $amount = $request->payable;

            $amount = 2;

            try{

                $order_id = 'Nolla'.mt_rand(1111,9999);
                $first_name = Auth::user()->name;
                $email = Auth::user()->email;
                $mobile = Auth::user()->phone;
                $amount = $amount;
                $desc = 'Order Charge';

                $merchant_id = Setting::get('merchant_id');
                $verifykey = Setting::get('verify_key');

                $vcode =  md5($amount.$merchant_id.$order_id.$verifykey);

                $url = url('/molpay/view').'?amount='.$amount.'&order_id='.$order_id.'&first_name='.$first_name.'&email='.$email.'&mobile='.$mobile.'&desc='.$desc.'&merchant_id='.$merchant_id.'&verifykey='.$verifykey.'&vcode='.$vcode;

                // dd($url);

                $molpay = new MolpayDetail();

                $molpay->amount = $amount;
                $molpay->order_id = $order_id;
                $molpay->user_id = \Auth::user()->id;
                $molpay->request_id = 1;
                $molpay->order_data = json_encode($request->all());
                $molpay->paid = 0 ;
                if($request->ajax()){
                    $molpay->type="mobile";
                }else{
                    $molpay->type="web";
                }

                $molpay->save();
                if($request->ajax()){
                     return response()->json(['url'=>$url,'status'=>'true']);
                }

                return redirect($url);

            }catch(Exception $e){
               return $e->getMessage();
            }
        }
        if($request->payment_mode == 'stripe') {
            $StripeCharge = $request->payable *100;  //exit;
            try {

                if($request->card_id){
                    $Card = Card::where('user_id',Auth::user()->id)->where('id',$request->card_id)->first();
                }else{
                    $Card = Card::where('user_id',Auth::user()->id)->where('is_default',1)->first();
                }
                Stripe::setApiKey(Setting::get('stripe_secret_key'));
                $Charge = Charge::create(array(
                      "amount" => (int)$StripeCharge,
                      "currency" => strtolower(Setting::get('currency_code')),
                      "customer" => Auth::user()->stripe_cust_id,
                      "card" => $Card->card_id,
                      "description" => "Payment Charge for ".Auth::user()->email,
                      "receipt_email" => Auth::user()->email
                ));
                return $Charge;

            } catch(StripeInvalidRequestError $e){
                return $e->getMessage();
            } catch(Exception $e) {
                return $e->getMessage();
            }
        }
        if($request->payment_mode == 'braintree') {
            $StripeCharge = $request->payable ;
            try {
                $this->set_Braintree();
              if($request->has('payment_method_nonce')){
                  if($request->payment_card=='PayPalAccount'){
                      $result = Braintree_Transaction::sale([
                        "amount" => $StripeCharge,
                        "paymentMethodNonce" => $request->payment_method_nonce,
                        "orderId" => uniqid(),
                        "merchantAccountId" => Setting::get('BRAINTREE_MERCHANT_ACCOUNT_ID'),
                        "options" => [
                          "paypal" => [
                            "customField" => '',
                            "description" => '',
                          ],
                        ],
                      ]);
                      dd($result);
                  }
                  // any card other than your
                  if($request->payment_card=='CreditCard'){
                      $result = Braintree_Transaction::sale([
                          'amount' => $StripeCharge,
                          'paymentMethodNonce' => $request->payment_method_nonce,
                          "merchantAccountId" => Setting::get('BRAINTREE_MERCHANT_ACCOUNT_ID')
                          /*'options' => [
                              'threeDSecure' => [
                                  'required' => true
                              ]
                          ]*/
                      ]);
                  }
              }else{
                // your card 
                    if($request->card_id){
                        $Card = Card::where('user_id',Auth::user()->id) ->where('card_type','braintree')->where('id',$request->card_id)->first();
                    }else{
                        $Card = Card::where('user_id',Auth::user()->id) ->where('card_type','braintree')->where('is_default',1)->first();
                    }
                    
                    // payment using card
                        $result = Braintree_Transaction::sale([
                          'amount' => $StripeCharge,
                          'paymentMethodToken' => $Card->card_id,
                          "merchantAccountId" => Setting::get('BRAINTREE_MERCHANT_ACCOUNT_ID')
                        ]);
                }
                if ($result->success) {
                    return $result->transaction;
                } 

            } catch(Braintree_Exception_NotFound $e){
                    return $e->getMessage();
            } catch(Exception $e) {
                    return $e->getMessage();   
            }
        }
    }


    /**
     * add wallet money for user.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_money(Request $request){


        if($request->has('payment_mode')&&$request->payment_mode=='RAZER'){

            $this->validate($request, [
                'amount' => 'required|integer'
            ]);

            $order_id = 'Nolla'.mt_rand(1111,9999);
            $first_name = Auth::user()->name;
            $email = Auth::user()->email;
            $mobile = Auth::user()->phone;
            $amount = $request->amount;
            $desc = 'Wallet Recharge';

            $merchant_id = Setting::get('merchant_id');
            $verifykey = Setting::get('verify_key');

            $vcode =  md5($amount.$merchant_id.$order_id.$verifykey);

            $url = url('/molpay/view').'?amount='.$amount.'&order_id='.$order_id.'&first_name='.$first_name.'&email='.$email.'&mobile='.$mobile.'&desc='.$desc.'&merchant_id='.$merchant_id.'&verifykey='.$verifykey.'&vcode='.$vcode;

           // dd($url);

            $molpay = new MolpayDetail();

            $molpay->amount = $amount;
            $molpay->order_id = $order_id;
            $molpay->user_id = \Auth::user()->id;
            $molpay->paid = 0 ;
            if($request->ajax()){
                $molpay->type="mobile";
            }else{
                $molpay->type="web";
            }

            $molpay->save();
            if($request->ajax()){
                 return response()->json(['url'=>$url,'status'=>'true']);
            }

            return redirect($url);

        }

        $this->validate($request, [
                'amount' => 'required|integer',
                'card_id' => 'required|exists:cards,id,user_id,'.Auth::user()->id
            ]);

        try{
            if(Setting::get('payment_mode')=='braintree'){

            }else{
            $StripeWalletCharge = $request->amount * 100;

            Stripe::setApiKey(Setting::get('stripe_secret_key'));
            $card =Card::where('id',$request->card_id)->first();
            $Charge = Charge::create(array(
                  "amount" => $StripeWalletCharge,
                  "currency" => "usd",
                  "customer" => Auth::user()->stripe_cust_id,
                  "card" => $card->card_id,
                  "description" => "Adding Money for ".Auth::user()->email,
                  "receipt_email" => Auth::user()->email
                ));

            $update_user = User::find(Auth::user()->id);
            $update_user->wallet_balance += $request->amount;
            $update_user->save();
            $update_user->currency = Setting::get('currency');
            $update_user->payment_mode = Setting::get('payment_mode');

            WalletPassbook::create([
              'user_id' => Auth::user()->id,
              'amount' => $request->amount,
              'message' => 'Adding Money form card '.$request->card_id,
              'status' => 'CREDITED',
              'via' => 'CARD',
            ]);

            Card::where('user_id',Auth::user()->id)->update(['is_default' => 0]);
            Card::where('id',$request->card_id)->update(['is_default' => 1]);
          }
            //sending push on adding wallet money
            (new SendPushNotification)->WalletMoney(Auth::user()->id,currencydecimal($request->amount));

            if($request->ajax()){
                  return  $update_user;
                /*return response()->json(['message' => Setting::get('currency').$request->amount.'  '.trans('order.payment.added_to_your_wallet'), 'user' => $update_user]); */
            } else {
                return back()->with('flash_success',$request->amount.trans('order.payment.added_to_your_wallet'));
            }

        } catch(StripeInvalidRequestError $e) {
            if($request->ajax()){
                 return response()->json(['error' => $e->getMessage()], 500);
            }else{
                return back()->with('flash_error',$e->getMessage());
            }
        } catch(Exception $e) {
            if($request->ajax()) {
                return response()->json(['error' => $e->getMessage()], 500);
            } else {
                return back()->with('flash_error', $e->getMessage());
            }
        }
    }



    public function molpay_view(Request $request){

        $order_id = $request->order_id;
        $first_name = $request->first_name;
        $email = $request->email;
        $mobile = $request->mobile;
        $amount = $request->amount;

        $desc = $request->desc;

        $merchant_id = Setting::get('merchant_id');
        $verifykey = Setting::get('verify_key');

        $vcode =  md5($amount.$merchant_id.$order_id.$verifykey);

        return view('user.molpay',compact('order_id','first_name','email','mobile','desc','amount','vcode'));

    }


    public function set_Braintree(){

       \Braintree_Configuration::environment(Setting::get('BRAINTREE_ENV'));
        \Braintree_Configuration::merchantId(Setting::get('BRAINTREE_MERCHANT_ID'));
        \Braintree_Configuration::publicKey(Setting::get('BRAINTREE_PUBLIC_KEY'));
        \Braintree_Configuration::privateKey(Setting::get('BRAINTREE_PRIVATE_KEY'));
    }

    public function checkRipplePayment(Request $request){

        $this->validate($request, [
                'payment_id' => 'required|unique:order_invoices',
                'amount' => 'required'
            ],[
              'payment_id.unique' => 'The Transaction Id has already been used.',
            ]);

      $transaction_id = $request->payment_id;
      $ripple_id = Setting::get('RIPPLE_KEY');
      $amount_ripple = $request->amount;

       try{ 
        $client = new \GuzzleHttp\Client();
        $request = $client->get('https://data.ripple.com/v2/transactions/'.$transaction_id.'?binary=false');
        $response = json_decode($request->getBody());

        if($response->result == 'success'){ 
            //echo $response->transaction->meta->delivered_amount;exit;
            if($response->transaction->tx->Destination==$ripple_id ){
             // echo $response->transaction->tx->Amount/1000000; echo '<br/>';
             // echo $amount_ripple; echo '<br/>';
              //echo ($response->transaction->tx->Amount/1000000 - $amount_ripple); echo '<br/>';
                if(($response->transaction->tx->Amount/1000000 - $amount_ripple) > -0.5){
                  return response()->json(['success' => 'Ok'], 200);
                }else{
                    return response()->json(['success' => 'Ok'], 200);
                  return response()->json(['error' => 'price_not_match'], 200);
                }
            }else{
              return response()->json(['error' => 'Failed'], 200);
            }
        }else{
          return response()->json(['error' => 'Failed'], 200);
        }

      }catch(Exception $e){
        return response()->json(['error' => 'id_not_valid'], 200);
      }
    }

    public function checkEtherPayment(Request $request){

        $this->validate($request, [
                'payment_id' => 'required|unique:order_invoices',
                'amount' => 'required'
            ],[
              'payment_id.unique' => 'The Transaction Id has already been used.',
            ]);

      $transaction_id = $request->payment_id;
      $ether_id = Setting::get('ETHER_KEY');
      $ether_admin_id = Setting::get('ETHER_ADMIN_KEY');
      $amount_ether = $request->amount;

       try{ 
        $client = new \GuzzleHttp\Client();
        $request = $client->get('http://api.etherscan.io/api?module=account&action=txlist&address='.Setting::get('ETHER_ADMIN_KEY').'&startblock=0&endblock=99999999&sort=asc&apikey='.Setting::get('ETHER_KEY'));
        $response = json_decode($request->getBody());
        $ether_data = '';
        if($response->message == 'OK'){ 
          foreach($response->result  as $ether){
            if($ether->blockHash==$transaction_id){ 
              $ether_data = $ether;
            }
          }
            //echo $response->transaction->meta->delivered_amount;exit;
            if($ether_data!=''){
                if($ether_data->to==$ether_admin_id){ 
                    if(($ether->value/1000000000000000000 - $amount_ether) > -0.005){
                      return response()->json(['success' => 'Ok'], 200);
                    }else{
                      return response()->json(['error' => 'price_not_match'], 200);
                    }
                }else{
                  return response()->json(['error' => 'Failed'], 200);
                }
            }else{
              return response()->json(['error' => 'id_not_valid'], 200);
            }
         
        }else{
          return response()->json(['error' => 'id_not_valid'], 200);
        }

      }catch(Exception $e){
        return response()->json(['error' => 'id_not_valid'], 200);
      }
    }


    public function molpay_return(Request $request){

        try{

            \Log::info($request->all());


           // dd($request->all());

            $vkey =  Setting::get('secret_key');

            $tranID = $request->tranID;
            $orderid = $request->orderid; 
            $status = $request->status; 
            $merchant = $request->domain; 
            $amount = $request->amount; 

            $currency = $request->currency; 
            $appcode = $request->appcode; 
            $paydate = $request->paydate; 
            $skey = $request->skey;


            $key0 = md5( $tranID.$orderid.$status.$merchant.$amount.$currency );
            $key1 = md5( $paydate.$merchant.$key0.$appcode.$vkey );


            if( $skey === $key1 ){
                

                $molpay = MolpayDetail::where('order_id',$request->orderid)->first();

                if(isset($molpay)){

                    $molpay->tranID =$tranID;
                    $molpay->orderid =$orderid;
                    $molpay->status = $status;
                    $molpay->currency =$currency;
                    $molpay->appcode =$appcode;
                    $molpay->paydate =$paydate;

                    $molpay->error_code = @$request->error_code;
                    $molpay->error_desc = @$request->error_desc;

                    $molpay->save();

                    if($molpay->paid==0){
                        Auth::loginUsingId($molpay->user_id);
                        if($molpay->request_id==''){
                           //wallet
                            if($request->status=="00" && $request->error_code=='' && $molpay->paid==0){


                                $update_user = User::find($molpay->user_id);
                                $update_user->wallet_balance += $molpay->amount;
                                $update_user->save();
                                $update_user->currency = Setting::get('currency');
                                $update_user->payment_mode = Setting::get('payment_mode');

                                WalletPassbook::create([
                                  'user_id' => $molpay->user_id,
                                  'amount' => $molpay->amount,
                                  'message' => 'Adding Money form Razer-Molpay ',
                                  'status' => 'CREDITED',
                                  'via' => 'CARD',
                                ]);

                                //sending push on adding wallet money
                                (new SendPushNotification)->WalletMoney($molpay->user_id,currencydecimal($molpay->amount));


                                $molpay->paid =1 ;
                                $molpay->save();

                                if($request->ajax()){
                                      return  $update_user;
                                } else {
                                    return redirect('payments')->with('flash_success',$molpay->amount.trans('order.payment.added_to_your_wallet'));
                                }


                                 
                            }else if($request->status=="22" && $request->error_code=='' && $molpay->paid==0){
                                //payment delay

                                $update_user = User::find($molpay->user_id);
                                
                                $update_user->currency = Setting::get('currency');
                                $update_user->payment_mode = Setting::get('payment_mode');

                                
                                //sending push on adding wallet money
                                (new SendPushNotification)->WalletMoney($molpay->user_id,currencydecimal($molpay->amount));


                                if($request->ajax()){
                                      return  $update_user;
                                } else {
                                    return redirect('payments')->with('flash_success',$molpay->amount.trans('order.payment.added_to_your_wallet'));
                                }




                            }else if($molpay->paid==1){
                                //payment paid already
                                if($request->ajax()){
                                    return response()->json(['message' => currency($molpay->amount).trans('api.added_to_your_wallet'), 'user' => $update_user]); 
                                } else {
                                 //   dd('emp');
                                    return redirect('payments')->with('flash_success',currency($molpay->amount).' added to your wallet Already');
                                }

                            }else{
                                //payment faled
                                if($request->ajax()){
                                    return response()->json(['message' => $request->error_desc,'status'=>'false']); 
                                } else {
                                    return redirect('payments')->with('flash_error',$request->error_desc);
                                }
                            }


                        }else{
                            //order

                            if($request->status=="00" && $request->error_code=='' && $molpay->paid==0){


                                    $req = json_decode($molpay->order_data,true);

                                    foreach ($req as $key => $value) {
                                      $request[$key]=$value;
                                    }

                                    $request['payment_mode']='razer_success'; 
                                    $request['pay_id']=$molpay->order_id;

                                    if($molpay->type=='mobile'){

                                       return response()->json(['status'=>true,'message' => 'Payment Success','pay_id'=>$molpay->order_id]);

                                    }

                                    $molpay->paid =1 ;
                                    $molpay->save();

                                    \Log::info("success order");

                                    return (new OrderResource)->store($request);


                            }else if($request->status=="22" && $request->error_code=='' && $molpay->paid==0){

                                //payment delay

                            }else if($molpay->paid==1){
                                //payment paid already
                                if($request->ajax()){
                                    return response()->json(['status'=>false,'message' => 'Payment Already Paid','pay_id'=>$molpay->order_id],422);
                                } else {
                                 //   dd('emp');
                                    return redirect('payments')->with('flash_error','Payment Already Paid');
                                }

                            }else{


                                if($request->ajax()){
                                   return response()->json(['message' => $request->error_desc,'status'=>'false']); 
                                } else {
                                   return redirect('payments')->with('flash_error',$request->error_desc);
                                }

                            }
                            

                        }

                    }else{
                        //already paid error

                        if($request->ajax()){
                            return response()->json(['message' => currency($molpay->amount).trans('api.added_to_your_wallet'), 'user' => $update_user]); 
                        } else {
                         //   dd('emp');
                            return redirect('payments')->with('flash_success',currency($molpay->amount).' added to your wallet Already');
                        }
                    }
                   
                }else{
                    //payment record not found

                    if($request->ajax()){
                        return response()->json(['message' =>'Payment Record Not Found'],422); 
                    } else {
                     //   dd('emp');
                        return redirect('payments')->with('flash_error','Payment Record Not Found');
                    }
                }
                
            }else{
                //payment mismatch

                if($request->ajax()){
                    return response()->json(['message' =>'Payment Data Error'],422); 
                } else {
                 //   dd('emp');
                    return redirect('payments')->with('flash_error','Payment Data Error');
                }
                
            }



        }catch(Exception $e){
\Log::info($e);

            if($request->ajax()){
                return response()->json(['error' =>$e->getMessage()],500); 
            } else {
             //   dd('emp');
                return redirect('payments')->with('flash_error',$e->getMessage());
            }

        }

    }


    public function molpay_return_callback(Request $request){
      \Log::info("Molpay Callback");
      \Log::info($request->all());

        try{

            $mol = MolpayDetail::where('tranID',$request->tranID)->first();
            $mol_count= count($mol);

            if($mol_count==1){
               if($mol->status=="22" && $mol->error_code=='' && $mol->paid==0){

                    if($request->status=="00"){

                       if($mol->request_id!=""&&isset($mol->request_id)){
                       //order request

                             $mol->status="00";
                             $mol->paid+= 1;
                             $mol->save();

                             

                          


                       }else if($mol->request_id==null){
                        //userwallet
                            $mol->status="00";
                            $mol->paid+= 1;
                            $mol->save();

                            $update_user = User::find($mol->user_id);
                            $update_user->wallet_balance += $mol->amount;
                            $update_user->save();
                            $update_user->currency = Setting::get('currency');
                            $update_user->payment_mode = Setting::get('payment_mode');

                            WalletPassbook::create([
                              'user_id' => $mol->user_id,
                              'amount' => $mol->amount,
                              'message' => 'Adding Money form Razer-Molpay ',
                              'status' => 'CREDITED',
                              'via' => 'CARD',
                            ]);

                            //sending push on adding wallet money
                            (new SendPushNotification)->WalletMoney($mol->user_id,currencydecimal($mol->amount));

                       }


                    }else{

                       if($mol->request_id!=""&&isset($mol->request_id)){
                        //ride request failed

                             (new SendPushNotification)->sendPushToUser($mol->user_id,"Your Order Failed amount".$mol->amount. "Please pay again");



                       }elseif($mol->request_id==null){

                             (new SendPushNotification)->sendPushToUser($mol->user_id,"Your Wallet Recharge Failed .Amount ".$mol->amount. "Please pay again for credit");


                       }



                    }


               }
            }else{

                if($mol->request_id!=""&&isset($mol->request_id)){
                        //ride request failed

                             (new SendPushNotification)->sendPushToUser($mol->user_id,"Your Ride Transaction Failed amount".$mol->amount. "Please pay again");



                       }elseif($mol->request_id==null){

                             (new SendPushNotification)->sendPushToUser($mol->user_id,"Your Wallet Recharge Failed .Amount ".$mol->amount. "Please pay again for credit");


                       }

            }


        }catch(Exception $e){
            \Log::info($e);
            return $e->getMessage();

        }
    }


    public function molpay_return_api(Request $request){

            $tranID = $request->txn_ID;
            $orderid = $request->order_id; 
            $status = $request->status_code; 
            $amount = $request->amount; 
            $appcode = $request->app_code; 
        
            $molpay = new MolpayDetail();

            $molpay->tranID =$tranID;
            $molpay->orderid =$orderid;
            $molpay->status = $status;
            $molpay->amount = $amount;
            $molpay->appcode =$appcode;
            $molpay->user_id = \Auth::user()->id;
            $molpay->save();


            if($status=='00'){

                $update_user = User::find($molpay->user_id);
                $update_user->wallet_balance += $molpay->amount;
                $update_user->save();
                $update_user->currency = Setting::get('currency');
                $update_user->payment_mode = Setting::get('payment_mode');

                WalletPassbook::create([
                  'user_id' => $molpay->user_id,
                  'amount' => $molpay->amount,
                  'message' => 'Adding Money form Razer-Molpay ',
                  'status' => 'CREDITED',
                  'via' => 'CARD',
                ]);

                //sending push on adding wallet money
                (new SendPushNotification)->WalletMoney($molpay->user_id,currencydecimal($molpay->amount));


                $molpay->paid =1 ;
                $molpay->save();

                if($request->ajax()){
                      return  $update_user;
                } else {
                    return redirect('payments')->with('flash_success',$molpay->amount.trans('order.payment.added_to_your_wallet'));
                }

            }else if($status=='22'){

                return response()->json(['status'=>true,'message' => 'Payment Pending','pay_id'=>$molpay->order_id ]);

            }else{


                if($request->ajax()){
                    return response()->json(['message' =>'Payment Failed'],422); 
                } else {
                 //   dd('emp');
                    return redirect('payments')->with('flash_error','Payment Data Error');
                }

            }



    }


    public function molpay_return_api_flow(Request $request){

            $tranID = $request->txn_ID;
            $orderid = $request->order_id; 
            $status = $request->status_code; 
            $amount = $request->amount; 
            $appcode = $request->app_code; 
        
            $molpay = new MolpayDetail();

            $molpay->tranID =$tranID;
            $molpay->orderid =$orderid;
            $molpay->status = $status;
            $molpay->amount = $amount;
            $molpay->appcode =$appcode;
            $molpay->user_id = \Auth::user()->id;
            $molpay->save();


            if($status=='00'){

                $molpay->paid =1 ;
                $molpay->save();

                return response()->json(['status'=>true,'message' => 'Payment Success','pay_id'=>$molpay->order_id , 'payment_mode'=>'razer_success']);

            }else if($status=='22'){

                return response()->json(['status'=>true,'message' => 'Payment Pending','pay_id'=>$molpay->order_id ]);

            }else{


                if($request->ajax()){
                    return response()->json(['message' =>'Payment Failed'],422); 
                } else {
                 //   dd('emp');
                    return redirect('payments')->with('flash_error','Payment Data Error');
                }

            }



    }
}
