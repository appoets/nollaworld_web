@extends('admin.layouts.app')

@section('content')

<!-- File export table -->
<div class="row file">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header">
                @if(Setting::get('DEMO_MODE')==0)
                    <div class="col-md-12" style="height:50px;color:red;">
                        ** Demo Mode : No Permission to Edit and Delete.
                    </div>
                @endif
                <h4 class="card-title">@lang('inventory.shoptype.title')</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        <li><a href="{{ route('admin.shoptypes.create') }}" class="btn btn-primary add-btn btn-darken-3">@lang('inventory.shoptype.add_shoptype')</a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard table-responsive">
                    <table class="table table-striped table-bordered file-export">
                        <thead>
                            <tr>
                                <th>@lang('inventory.shoptype.sl_no')</th>
                                <th>@lang('inventory.shoptype.name')</th>
                                <th>@lang('inventory.shoptype.action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($ShopTypes as $key=>$Shoptype)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$Shoptype->name}}</td>
                                    <td>
                                        @if(Setting::get('DEMO_MODE')==1)
                                            <a href="{{ route('admin.shoptypes.edit', $Shoptype->id) }}" class="table-btn btn btn-icon btn-success"><i class="fa fa-pencil-square-o"></i></a>
                                                    
                                            <button  class="table-btn btn btn-icon btn-danger" form="resource-delete-{{ $Shoptype->id }}" ><i class="fa fa-trash-o" onclick="return confirm('Are you sure you want to delete this shoptype?');"></i></button> 
                                        @endif
                                        <form id="resource-delete-{{ $Shoptype->id }}" action="{{ route('admin.shoptypes.destroy', $Shoptype->id)}}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                        </form>
                                    </td>
                                </tr>
                            @empty
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <h4>@lang('inventory.shoptype.no_record_found')</h4>
                                </div>
                            </div>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- File export table -->
@endsection
