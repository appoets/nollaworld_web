<?php

namespace App\Http\Controllers\Resource;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Route;
use Setting;
use App\ShopType;

class ShopTypeResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ShopTypes = ShopType::get();

        return view(Route::currentRouteName(), compact('ShopTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(Route::currentRouteName());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'status' => 'required',
        ]);

        try {
            $ShopType = $request->all();
            $ShopType = ShopType::create($ShopType);
            
            return back()->with('flash_success', 'Shop Type added!');
        } catch (Exception $e) {
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $ShopType = ShopType::findOrFail($id);
            
            return view(Route::currentRouteName(), compact('ShopType'));
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', trans('form.whoops'));
        } catch (Exception $e) {
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        try {
            $ShopType = ShopType::findOrFail($id);
            
            return view(Route::currentRouteName(), compact('ShopType'));
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', trans('form.whoops'));
        } catch (Exception $e) {
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'status' => 'required'
        ]);

        try {
            $ShopType = ShopType::findOrFail($id);
            $Update = $request->all();
            $ShopType->update($Update);

            return back()->with('flash_success', 'Shop Type updated!');
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', trans('form.whoops'));
        } catch (Exception $e) {
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $ShopType = ShopType::findOrFail($id);
            $ShopType->delete();

            return back()->with('flash_success', 'Shop Type deleted!');
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', trans('form.whoops'));
        } catch (Exception $e) {
            return back()->with('flash_error', trans('form.whoops'));
        }
    }
}
