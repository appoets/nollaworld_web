@extends('admin.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">@lang('inventory.shoptypecommision.add_title')</h3>
    </div>
    <div class="card-body collapse in">
        <div class="card-block">
            <form role="form" method="POST" action="{{ route('admin.shoptypecommision.store') }}" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('shop_id') ? ' has-error' : '' }}">
                    <label for="parent_id">@lang('inventory.shoptypecommision.shop_name'): @if(Request::get('shop')) {{\App\Shop::find(Request::get('shop'))->name}} @endif</label>
                    <input name="shop_id" type="hidden" value="{{Request::get('shop')}}" />
                    @if ($errors->has('shop_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('shop_id') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('shoptype') ? ' has-error' : '' }}">
                    <label for="shoptype">@lang('inventory.shoptypecommision.shoptype')</label>

                    <select class="form-control" id="shop_type_id" name="shop_type_id" required>
                        <option value="" selected>--Select Type--</option>
                        @forelse($ShopType as $shoptype)
                        <option value="{{ $shoptype->id }}">{{ $shoptype->name }}</option>
                        @empty
                        <option value="0">None</option>
                        @endforelse
                    </select>

                    @if ($errors->has('shoptype'))
                        <span class="help-block">
                            <strong>{{ $errors->first('shoptype') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('commision') ? ' has-error' : '' }}">
                    <label for="commision">@lang('inventory.shoptypecommision.commision')</label>
                    <input type="text" class="form-control" id="commision" name="commision" required/>
                        @if ($errors->has('commision'))
                            <span class="help-block">
                                <strong>{{ $errors->first('commision') }}</strong>
                            </span>
                        @endif
                </div>
                <div class="col-xs-12 mb-2">
                    <a href="{{ route('admin.shoptypecommision.index') }}?shop={{Request::get('shop')}}" class="btn btn-warning mr-1">
                        <i class="ft-x"></i> Cancel
                    </a>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-check-square-o"></i> Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
