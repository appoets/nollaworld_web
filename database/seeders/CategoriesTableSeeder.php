<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	DB::table('categories')->truncate();
        DB::table('categories')->insert([
            [
                'name' => 'Nuts,Coconut and Fruit',
                'shop_id' => 1,
                'description' => 'special'
            ],
            [
                'name' => 'Pantry',
                'shop_id' => 1,
                'description' => 'special'
            ],
            [
                'name' => 'Fruits and Vegetables',
                'shop_id' => 1,
                'description' => 'special'
            ],
			            [
                'name' => 'Bakery',
                'shop_id' => 2,
                'description' => 'special'
            ],
            [
                'name' => 'Cleaning and Household Essentials ',
                'shop_id' => 2,
                'description' => 'special'
            ],
            [
                'name' => 'Frozen',
                'shop_id' => 2,
                'description' => 'special'
            ],
             [
                'name' => 'Beverages',
                'shop_id' => 3,
                'description' => 'special'
            ],
            [
                'name' => 'Healthy and Beauty',
                'shop_id' => 3,
                'description' => 'special'
            ],
            [
                'name' => 'Baby and Kids',
                'shop_id' => 3,
                'description' => 'special'
            ],
             [
                'name' => 'Meat',
                'shop_id' => 4,
                'description' => 'special'
            ],
            [
                'name' => 'Dairy Products',
                'shop_id' => 4,
                'description' => 'special'
            ],
            [
                'name' => 'Pantry',
                'shop_id' => 4,
                'description' => 'special'
            ],
            [
                'name' => 'Flowers',
                'shop_id' => 5,
                'description' => 'special'
            ],
            [
                'name' => 'Breakfast & Cereal',
                'shop_id' => 5,
                'description' => 'special'
            ],
            [
                'name' => 'Beverages',
                'shop_id' => 5,
                'description' => 'special'
            ],
            
         ]);
    }
}
