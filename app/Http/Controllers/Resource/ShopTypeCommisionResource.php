<?php

namespace App\Http\Controllers\Resource;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Route;
use Exception;
use App\ShopType;
use App\Shop;
use App\ShopTypeCommision;

class ShopTypeCommisionResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $ShopTypeCommision = ShopTypeCommision::with('shoptype', 'shop')
            ->where('shop_id',$request->shop)->get();
        //dd($ShopTypeCommision);
        return view(Route::currentRouteName(), compact('ShopTypeCommision'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $Shop = Shop::where('id',$request->shop)->get();
        $ShopType = ShopType::get();

        return view(Route::currentRouteName(), compact('ShopType','Shop'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'shop_id' => 'required',
            'shop_type_id' => 'required',
            'commision' => 'required'
        ]);

        try {
            $ShopTypeCommision = $request->all();
            $ShopTypeCommision = ShopTypeCommision::create($ShopTypeCommision);

            return back()->with('flash_success', 'Commision added!');
        } catch (Exception $e) {
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        try {
            $ShopTypeCommision = ShopTypeCommision::findOrFail($id);
            $ShopType = ShopType::get();
            
            return view(Route::currentRouteName(), compact('ShopTypeCommision','ShopType'));
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', trans('form.whoops'));
        } catch (Exception $e) {
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        try {
            $ShopTypeCommision = ShopTypeCommision::findOrFail($id);
            $ShopType = ShopType::get();

            return view(Route::currentRouteName(), compact('ShopTypeCommision','ShopType'));
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', trans('form.whoops'));
        } catch (Exception $e) {
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'shop_id' => 'required',
            'shop_type_id' => 'required',
            'commision' => 'required'
        ]);

        try {
            $ShopTypeCommision = ShopTypeCommision::findOrFail($id);
            $Update = $request->all();
            $ShopTypeCommision->update($Update);

            return back()->with('flash_success', 'Commision updated!');
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', trans('form.whoops'));
        } catch (Exception $e) {
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $ShopTypeCommision = ShopTypeCommision::findOrFail($id);
            $ShopTypeCommision->delete();

            return back()->with('flash_success', 'Commision deleted!');
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', trans('form.whoops'));
        } catch (Exception $e) {
            return back()->with('flash_error', trans('form.whoops'));
        }
    }
}
