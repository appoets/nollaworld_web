<?php

return [
  'gcm' => [
      'priority' => 'high',
      'dry_run' => false,
      'apiKey' => env('ANDROID_PUSH_KEY', 'yourAPIKey'),
  ],
  'fcm' => [
        'priority' => 'high',
        'dry_run' => false,
        'apiKey' => env('ANDROID_PUSH_KEY', 'yourAPIKey'),
  ],
  'apn' => [
      
  ]
];
