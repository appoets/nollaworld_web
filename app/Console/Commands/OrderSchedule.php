<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\Http\Controllers\Resource\OrderResource;
use App\Http\Controllers\AdminController;
use App\Order;
use App\OrderTiming;

use App\Shop;
use App\Http\Controllers\SendPushNotification;

class OrderSchedule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:orderschedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $Order = Order::wherestatus('SCHEDULED')->where('delivery_date','<=',\Carbon\Carbon::now()->addMinutes(20))->get();

        foreach ($Order as $key => $value) {
            \Log::info("schedule success"); 

            $Update = OrderTiming::whereorder_id($value->id)->first(); 
            $Update->delete();
                     
            $value->status = 'ORDERED';
            $value->save();
                
            OrderTiming::create([
                'order_id' => $value->id,
                'status' => 'ORDERED'
            ]);

            $User = $value->user_id;

            $Shop = Shop::where('status','active')->findOrFail($value->shop_id);

            $push_message = trans('order.order_created',['id'=>$value->id]);
            $order = Order::find($value->id);
            (new SendPushNotification)->sendPushToUser($User,$push_message,$page =['page'=>'userorder','status'=>$order->status,'dispute_status'=>$order->dispute,'order_id'=>$order->id]);
                 $push_message = 'New Incoming Order Received';
            (new SendPushNotification)->sendPushToShop($Shop->id,$push_message,$page =['page'=>'shoporder','status'=>$order->status,'dispute_status'=>$order->dispute,'order_id'=>$order->id]);
        }
    }
}
