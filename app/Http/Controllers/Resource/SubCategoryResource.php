<?php

namespace App\Http\Controllers\Resource;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Route;
use Setting;
use App\Category;
use App\SubCategory;

class SubCategoryResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $SubCategories = SubCategory::where('category_id',$request->category)->get();
        if($request->ajax()){
            return $SubCategories;
        }
        return view('admin.categories.sub_category.index', compact('SubCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $SubCategories = SubCategory::where('category_id',$request->category)->get();
        return view('admin.categories.sub_category.create', compact('SubCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'description' => 'required|max:1000',
            'status' => 'required|in:enabled,disabled',
            'category_id' => 'required'
        ]);

        try {
            $SubCategory = $request->all();

            $SubCategory = SubCategory::create($SubCategory);

            // return redirect()->route('admin.subcategories.index')->with('flash_success', 'SubCategory added!');
            return back()->with('flash_success', 'SubCategory added!');
        } catch (Exception $e) {
            // return redirect()->route('admin.subcategories.index')->with('flash_error', trans('form.whoops'));
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        try {
            $SubCategory = SubCategory::findOrFail($id);

            return view('admin.categories.sub_category.edit', compact('SubCategory'));
        } catch (ModelNotFoundException $e) {
            // return redirect()->route('admin.subcategories.index')->with('flash_error', 'SubCategory not found!');
            return back()->with('flash_error', trans('form.whoops'));
        } catch (Exception $e) {
            // return redirect()->route('admin.subcategories.index')->with('flash_error', trans('form.whoops'));
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'description' => 'required|max:1000',
            'status' => 'required|in:enabled,disabled'
        ]);

        try {
            $SubCategory = SubCategory::findOrFail($id);
            $Update = $request->all();
            $SubCategory->update($Update);

            // return redirect()->route('admin.subcategories.index')->with('flash_success', 'SubCategory updated!');
            return back()->with('flash_success', 'SubCategory updated!');
        } catch (ModelNotFoundException $e) {
            // return redirect()->route('admin.subcategories.index')->with('flash_error', 'SubCategory not found!');
            return back()->with('flash_error', trans('form.whoops'));
        } catch (Exception $e) {
            // return redirect()->route('admin.subcategories.index')->with('flash_error', trans('form.whoops'));
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $SubCategory = SubCategory::findOrFail($id);
            $SubCategory->delete();

            // return redirect()->route('admin.subcategories.index')->with('flash_success', 'SubCategory updated!');
            return back()->with('flash_success', 'SubCategory deleted!');
        } catch (ModelNotFoundException $e) {
            // return redirect()->route('admin.subcategories.index')->with('flash_error', 'SubCategory not found!');
            return back()->with('flash_error', trans('form.whoops'));
        } catch (Exception $e) {
            // return redirect()->route('admin.subcategories.index')->with('flash_error', trans('form.whoops'));
            return back()->with('flash_error', trans('form.whoops'));
        }
    }
}
