<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light osahan-nav shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="#"><img alt="logo" src="{{ asset(Setting::get('site_logo', 'logo.png')) }}"></a>
            <!--  -->

            <!-- <div class="d-flex align-items-center m-none">
                <div class="dropdown mr-3">
                    <a class="text-dark dropdown-toggle d-flex align-items-center py-3" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div><i class="fa fa-map-marker mr-2 bg-light rounded-pill p-2 icofont-size"></i></div>
                        <div>
                            <p class="text-muted mb-0 small">{{@Auth::user()->location}}</p>
                            
                        </div>
                    </a>
                    <div class="dropdown-menu p-0 drop-loc" aria-labelledby="navbarDropdown">
                        <div class="osahan-country">
                            <div class="search_location bg-primary p-3 text-right">
                                <div class="input-group rounded shadow-sm overflow-hidden">
                                    <div class="input-group-prepend">
                                        <button class="border-0 btn btn-outline-secondary text-dark bg-white btn-block"><i class="feather-search"></i></button>
                                    </div>
                                    <input type="text" class="shadow-none border-0 form-control" placeholder="Enter Your Location" autofocus="" autocomplete="on">
                                </div>
                            </div>
                            <div class="p-3 border-bottom">
                                <a href="/" class="text-decoration-none">
                                    <p class="font-weight-bold text-primary m-0"><i class="fa fa-map-marker"></i> New York, USA</p>
                                </a>
                            </div>
                            <div class="filter">
                                <h6 class="px-3 py-3 bg-light pb-1 m-0 border-bottom">Choose your country</h6>
                                <div class="custom-control  border-bottom px-0 custom-radio">
                                    <input type="radio" id="customRadio1" name="location" class="custom-control-input">
                                    <label class="custom-control-label py-3 w-100 px-3" for="customRadio1">Afghanistan</label>
                                </div>
                                <div class="custom-control  border-bottom px-0 custom-radio">
                                    <input type="radio" id="customRadio2" name="location" class="custom-control-input" checked="">
                                    <label class="custom-control-label py-3 w-100 px-3" for="customRadio2">India</label>
                                </div>
                                <div class="custom-control  border-bottom px-0 custom-radio">
                                    <input type="radio" id="customRadio3" name="location" class="custom-control-input">
                                    <label class="custom-control-label py-3 w-100 px-3" for="customRadio3">USA</label>
                                </div>
                                <div class="custom-control  border-bottom px-0 custom-radio">
                                    <input type="radio" id="customRadio4" name="location" class="custom-control-input">
                                    <label class="custom-control-label py-3 w-100 px-3" for="customRadio4">Australia</label>
                                </div>
                                <div class="custom-control  border-bottom px-0 custom-radio">
                                    <input type="radio" id="customRadio5" name="location" class="custom-control-input">
                                    <label class="custom-control-label py-3 w-100 px-3" for="customRadio5">Japan</label>
                                </div>
                                <div class="custom-control  px-0 custom-radio">
                                    <input type="radio" id="customRadio6" name="location" class="custom-control-input">
                                    <label class="custom-control-label py-3 w-100 px-3" for="customRadio6">China</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->


            <!--  -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <?php $setting = \App\Settings::where('key', 'CONTACT_NUMBER')->first(); ?>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                    </li>
                    {{--<li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-mobile-phone"></i> Phone <span class="badge badge-danger">{{$setting->value}}</span></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-language" aria-hidden="true"></i>
                            
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow-sm border-0">
                            <a class="dropdown-item" href="listing.html">English</a>
                           <!--  <a class="dropdown-item" href="detail.html">Language 2</a>
                            <a class="dropdown-item" href="checkout.html">Language 3</a> -->
                        </div>
                    </li>--}}
                    @if(Auth::guest())
                        <li class="nav-item"> <a class="nav-link" href="{{url('login')}}">Login</a></li>
                        <li class="nav-item"> <a class="nav-link" href="{{url('register')}}">Register</a></li>
                        <?php $cart_no =0; ?>
                    @else
                        <?php 
                        $cart = \App\UserCart::list(Auth::user()->id);
                        //dd($cart[0]->product->shop->name);
                        $cart_no = count($cart);?>
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="ion-ios-person"></i>
                                {{@Auth::user()->name}}<strong class="caret"></strong>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right shadow-sm border-0">
                                <a class="dropdown-item" href="{{url('/orders')}}">@lang('user.create.orders')</a>
                                <a class="dropdown-item" href="{{url('/offers')}}"><i class="fa fa-gift"></i> @lang('user.create.offers')</a>
                                <a class="dropdown-item" href="{{url('/payments')}}"><i class="fa fa-credit-card"></i> @lang('user.create.payments')</a>
                                <a class="dropdown-item" href="{{url('/favourite')}}"><i class="fa fa-heart-o"></i>@lang('user.create.fav')</a>
                                <a class="dropdown-item" href="{{url('/useraddress')}}"><i class="fa fa-map-marker"></i> @lang('user.create.address')</a>
                                <!-- <a class="dropdown-item" href="{{url('/referral')}}"><i class="fa fa-user-plus"></i> Referral</a> -->
                                <a  class="dropdown-item"  href="javascript:void(0);" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>
                                    @lang('menu.user.logout')</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    @endif
                        
                    <?php 
                        if($cart_no==0){
                            $url = url('restaurant/details?name='.@$Shop->name);
                        }else{
                            $url = url('restaurant/details?name='.@$cart[0]->product->shop->name.'&myaddress=home');
                        }
                    ?>

                    <li class="nav-item"> 
                        <a class="nav-link px-4" href="{{$url}}">
                            <span class="cart-count">{{@$cart_no}}</span>
                            <i class="fa fa-shopping-basket"></i>
                            <!-- <span class="badge badge-success">5</span> -->
                        </a>
                    </li>
                      
                    <!-- <li class="nav-item">
                        <a class="nav-link px-4" href="{{$url}}"><span class="cart-count">{{$cart_no}}</span> Cart</a>
                    </li> -->   
                </ul>
            </div>
        </div>
    </nav>
</header>
