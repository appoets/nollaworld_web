<?php
namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->truncate();
        DB::table('admins')->insert([
            [
                'name' => 'Admin',
                'email' => 'admin@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+911234565434'
            ]
        ]);
    }
}
