<!DOCTYPE html>
<html lang="en">
<head>
  <title>Nolla World</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="shortcut icon" href="{{ Setting::get('site_favicon', asset('favicon.ico')) }}">

  <!-- Bootstrap -->
  <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css')}}">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
  <!-- FA icons -->
  <link rel="stylesheet" href="{{ asset('assets/css/all.min.css')}}">
  <!-- Styles -->
  <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/css/responsive.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css')}}">
  <!-- <link rel="stylesheet" href="{{ asset('assets/css/owl.theme.min.css')}}"> -->
  <link rel="stylesheet" href="{{ asset('assets/css/owl.transitions.css')}}">

  <link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css')}}">

  <link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css')}}">
  <link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css')}}">
  <link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.transitions.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/user/css/style.css')}}">

    <link rel="stylesheet" type="text/css" href="https://askbootstrap.com/preview/osahan-eat/vendor/bootstrap/css/bootstrap.min.css">
 
</head>
@yield('styles')
<body>
@include('user.notification')

@include('user.layouts.partials.header')

@yield('content')
@include('user.layouts.partials.footer')

  <!-- Jquery -->
  <script src="{{ asset('assets/js/jquery.min.js')}}"></script>
<!-- desgin -->
  <script src="https://askbootstrap.com/preview/osahan-eat/vendor/jquery/jquery-3.3.1.slim.min.js"></script>
  <script src="https://askbootstrap.com/preview/osahan-eat/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- <script
      src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
      integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8="
      crossorigin="anonymous"></script> -->
      <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
  <!-- Bootstrap -->
  <script src="{{ asset('assets/js/popper.min.js')}}"></script>
  <script src="{{ asset('assets/js/bootstrap.min.js')}}"></script>

  <!-- Ionicons -->
  <script src="{{ asset('https://unpkg.com/ionicons@5.0.0/dist/ionicons.js')}}"></script>

  <!--slick carousel -->
<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js')}}"></script>
<script type="text/javascript" src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js')}}"></script>



  <!--slick carousel -->
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('https://unpkg.com/isotope-layout@3.0.4/dist/isotope.pkgd.min.js')}}"></script>
   <!-- Map JS -->
    <script src="{{  asset('assets/user/js/jquery.googlemap.js')}}"></script>
    
    @include('user.layouts.partials.script')

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
 <!-- Incrementing JS -->
 <script src="{{ asset('assets/user/js/incrementing.js')}}"></script>
  <!-- Scripts -->
  <script src="{{ asset('assets/user/js/scripts.js')}}"></script>
  <script src="{{ asset('assets/user/js/jquery.matchHeight-min.js')}}"></script>

@yield('scripts')

@yield('deliveryscripts')
<script type="text/javascript">
      $(function() {
      $('.equal-height').matchHeight({
      byRow: true,
      property: 'height'
      });
      });
    </script>

<script type="text/javascript">
    function googleTranslateElementInit() {
      new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
    }

    function triggerHtmlEvent(element, eventName) {
      var event;
      if (document.createEvent) {
        event = document.createEvent('HTMLEvents');
        event.initEvent(eventName, true, true);
        element.dispatchEvent(event);
      } else {
        event = document.createEventObject();
        event.eventType = eventName;
        element.fireEvent('on' + event.eventType, event);
      }
    }

    jQuery('.lang-select').click(function() {
      var theLang = jQuery(this).attr('data-lang');
      jQuery('.goog-te-combo').val(theLang);

      //alert(jQuery(this).attr('href'));
      window.location = jQuery(this).attr('href');
      location.reload();

    });
  </script>
  
 <script type="text/javascript">
$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1000,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination: false,
        navigation:true,
        navigationText:["",""],
        transitionStyle : "goDown",
        autoPlay:false
    });

    $(".fav-slider").owlCarousel({
        items:4,
        itemsDesktop:[1000,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination: false,
        navigation:true,
        navigationText:["",""],
        transitionStyle : "goDown",
        autoPlay:false
    });
});
</script>

 <script type="text/javascript">
   $(document).ready(function(){
    $('.customer-logos').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 2
            }
        }]
    });
});
 </script>

 <script type="text/javascript">

  //login page
   $(document).ready(function(){
        $(".signin-step2, .signin-step3").hide();
        $(".btn-login-next1").click(function(){
          event.preventDefault()
          $(".signin-step2").show();
          $(".signin-step1, .signin-step3").hide();
        });
        $(".btn-login-next2").click(function(){
          event.preventDefault()
          $(".signin-step3").show();
          $(".signin-step1, .signin-step2").hide();
        });
    });

   //register page
   $(document).ready(function(){
        $(".signup-step2, .signup-step3, .signup-step4").hide();
        $(".btn-reg-next1").click(function(){
          event.preventDefault()
          $(".signup-step2").show();
          $(".signup-step1, .signup-step3, .signup-step4").hide();
        });
        $(".btn-reg-next2").click(function(){
          event.preventDefault()
          $(".signup-step3").show();
          $(".signup-step1, .signup-step2, .signup-step4").hide();
        });
        $(".btn-reg-next3").click(function(){
          event.preventDefault()
          $(".signup-step4").show();
          $(".signup-step1, .signup-step2, .signup-step3").hide();
        });
    });
 </script>

<!-- image Upload -->
 <script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
      }
          }
      $("#imageUpload").change(function() {
          readURL(this);
      });
 </script>

 <!-- add card page -->
 <script type="text/javascript">
   $(document).ready(function(){
        $(".card-block-2").hide();
        $(".card-next-1").click(function(){
          event.preventDefault()
          $(".card-block-2").show();
          $(".card-block-1").hide();
        });
      });
 </script>
  
<!-- add address page -->
 <!-- <script type="text/javascript">
   $(document).ready(function(){
     $(".address-form").hide();
        $(".add-address").click(function(){
          event.preventDefault()
          $(".address-form").show();
          $(".address-box, .add-address-row").hide();
        });
      });
 </script> -->


  <script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();
    });
</script>

<script type="text/javascript">
  function increaseValue() {
  var value = parseInt(document.getElementById('number').value, 10);
  value = isNaN(value) ? 0 : value;
  value++;
  document.getElementById('number').value = value;
}

function decreaseValue() {
  var value = parseInt(document.getElementById('number').value, 10);
  value = isNaN(value) ? 0 : value;
  value < 1 ? value = 1 : '';
  value--;
  document.getElementById('number').value = value;
}

 function increaseValue1() {
  var value = parseInt(document.getElementById('number1').value, 10);
  value = isNaN(value) ? 0 : value;
  value++;
  document.getElementById('number1').value = value;
}

function decreaseValue1() {
  var value = parseInt(document.getElementById('number1').value, 10);
  value = isNaN(value) ? 0 : value;
  value < 1 ? value = 1 : '';
  value--;
  document.getElementById('number1').value = value;
}
</script>

<script type="text/javascript">
 
</script>


<!--Store Filter-->

<script type="text/javascript">
   $('.filters ul li').click(function(){
    $('.filters ul li').removeClass('active');
    $(this).addClass('active');
    
    var data = $(this).attr('data-filter');
    $grid.isotope({
      filter: data
    })
  });

  var $grid = $(".grid").isotope({
    itemSelector: ".all",
    percentPosition: true,
    masonry: {
      columnWidth: ".all"
    }
  })
</script>

<!--Search Dropdown-->
<script type="text/javascript">
  $('.dropdown-toggle').click(function(e) {
  e.preventDefault();
  e.stopPropagation();
  $(this).closest('.search-dropdown').toggleClass('open');
});

$('.dropdown-menu > li > a').click(function(e) {
  e.preventDefault();
  var clicked = $(this);
  clicked.closest('.dropdown-menu').find('.menu-active').removeClass('menu-active');
  clicked.parent('li').addClass('menu-active');
  clicked.closest('.search-dropdown').find('.toggle-active').html(clicked.html());
});

$(document).click(function() {
  $('.search-dropdown.open').removeClass('open');
});
</script>



<!-- Product image Script -->

<script type="text/javascript">
  $(".mini img").click(function(){  

 $(".maxi").attr("src",$(this).attr("src").replace("100x100","400x400"));

});
  $('.icon-wishlist').on('click', function(){
  $(this).toggleClass('in-wishlist');
});
</script>

<script type="text/javascript">
  $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    pagination:false,
    navigation:true,    
    navigationText:["",""],
    transitionStyle : "goDown",
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})
</script>
<script type="text/javascript">
  $(document).ready(function() {

$(window).resize(function(){
if ($(window).width() >= 980){

$(".navbar .dropdown-toggle").hover(function () {
$(this).parent().toggleClass("show");
$(this).parent().find(".dropdown-menu").toggleClass("show");
});

$( ".navbar .dropdown-menu" ).mouseleave(function() {
$(this).removeClass("show");
});

}
});



});
</script>

<script type="text/javascript">
  jQuery(document).on('click', '.mega-dropdown', function(e) {
  e.stopPropagation()
})
</script>
<style type="text/css">
  .product-category .navbar .dropdown-menu div[class*="col"] {
    margin-bottom: 1rem
}

.product-category .navbar .dropdown-menu {
    border: 1px solid rgba(0, 0, 0, .15);
    background-color: #fff;
    border: none;
    box-shadow: 0px 6px 3px rgb(51 51 51 / 30%);
}

.product-category .navbar-dark .navbar-nav .nav-link {
    color: #000;
}

.product-category .navbar {
    padding-top: 0px;
    padding-bottom: 0px
}

.product-category .navbar .nav-item {
    padding: .5rem .5rem;
    margin: 0 .25rem
}

.product-category .navbar .dropdown {
    position: static
}

.product-category .navbar .dropdown-menu {
    width: 100%;
    left: 0;
    right: 0;
    top: 45px
}

.product-category .navbar .dropdown:hover .dropdown-menu,
.product-category .navbar .dropdown .dropdown-menu:hover {
    display: block !important
}

.product-category .navbar .dropdown-menu {
    border: 1px solid rgba(0, 0, 0, .15);
    background-color: rgb(227 245 223);
}

li.nav-item {
    list-style-type: none;
}
.product-category a.i {
    margin-top: -20px;
    font-weight: 400;
    color: white
}

.product-category a.catogary {
    margin-left: -20px;
    color: #000 !important;
}
.product-category nav {
    background: transparent !important;
    box-shadow: none !important;
    border-bottom: 2px solid #3ec423;
}
.product-category a.navbar-brand {
    color: #3ec423 !important;
    padding: 10px;
    margin: 10px;
    display: inline-block;
    width: 100%;
}
button.srch {
    padding: 0px
}

@media only screen and (max-width: 1024px) and (min-width: 768px) {
    input.srch {
        padding: 0px;
        width: 100%
    }
}

.product-category a.nav-link.active {
    font-weight: 700;
    background-color: #3ec423;
    color: white;
}

.product-category a.navbar-brand {
    color: white
}

span.navbar-toggler.icon {
    color: black
}
</style>
<!-- css added -->
<style type="text/css">
   .dropdown-menu {
    font-size: 13px;
}
.alert, .badge, .dropdown-menu {
    border-radius: 2px;
}
.dropdown-cart-top {
    border-top: 2px solid #ff3008;
    min-width: 340px;
}
.p-0 {
    padding: 0!important;
}
.shadow-sm {
    box-shadow: 0 .125rem .25rem rgba(0,0,0,.075)!important;
}
.border-0 {
    border: 0!important;
}
.dropdown-menu-right {
    right: 0;
    left: auto;
}
.dropdown-cart-top-header {
    min-height: 107px;
}
.p-4 {
    padding: 1.5rem!important;
}
.dropdown-cart-top-header .img-fluid {
    border: 1px solid #dcdcdc;
    border-radius: 3px;
    float: left;
    height: 59px;
    padding: 3px;
    width: 59px;
}
.mr-3, .mx-3 {
    margin-right: 1rem!important;
}
.dropdown-cart-top-header h6 {
    font-size: 14px;
}
.mb-0, .my-0 {
    margin-bottom: 0!important;
}
.dropdown-cart-top-header p.text-secondary {
    font-size: 11px;
    line-height: 24px;
    font-weight: 600;
}
.text-secondary {
    color: #6c757d!important;
}
.p-4 {
    padding: 1.5rem!important;
}
.border-top {
    border-top: 1px solid #dee2e6!important;
}
.dropdown-cart-top-header p {
    font-size: 13px;
    color: #7a7e8a;
}
.float-right {
    float: right!important;
}
.drop-loc {
    width: 250px;
}
.p-3 {
    padding: 1rem!important;
}
.text-decoration-none {
    text-decoration: none!important;
}
.font-weight-bold {
    font-weight: 600 !important;
}
.pl-3, .px-3 {
    padding-left: 1rem!important;
}
.pl-0, .px-0 {
    padding-left: 0!important;
}
.custom-control-input {
    position: absolute;
    left: 0;
    z-index: -1;
    width: 1rem;
    height: 1.25rem;
    opacity: 0;
}
.w-100 {
    width: 100%!important;
}
.custom-control-label {
    position: relative;
    margin-bottom: 0;
    vertical-align: top;
}
.custom-control-label::before {
    position: absolute;
    top: 1.05rem;
    right: 1.05rem;
    left: auto;
    display: block;
    width: 1rem;
    height: 1rem;
    pointer-events: none;
    content: "";
    background-color: #fff;
    border: #adb5bd solid 1px;
}
.custom-control-label::after {
    position: absolute;
    top: .25rem;
    left: -1.5rem;
    display: block;
    width: 1rem;
    height: 1rem;
    content: "";
    background: no-repeat 50%/50% 50%;
}


.filter .custom-control-label::after {
    left: auto;
    right: 1rem;
    margin: auto;
    top: 0;
    bottom: 0;
}
.navbar-brand {
    display: inline-block;
    padding-top: .3125rem;
    padding-bottom: .3125rem;
    margin-right: 0px;
    font-size: 1.25rem;
    line-height: inherit;
    white-space: nowrap;
}
.nav-osahan-pic {
    width: 32px;
    height: 32px;
    border: 3px solid #fff;
    box-shadow: 0px 0px 3px #ccc;
    position: relative;
    margin: -8px 5px -6px 0;
    vertical-align: text-top;
}
.nav-link i {
    padding-right: 3px;
    font-size: 17px;
}
.rounded-pill {
    border-radius: 50rem!important;
}
 </style>

</body>
</html>
