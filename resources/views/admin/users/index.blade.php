@extends('admin.layouts.app')

@section('content')
<!-- File export table -->
<div class="row file">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header">
            @if(Setting::get('DEMO_MODE')==0)
            <div class="col-md-12" style="height:50px;color:red;">
                 ** Demo Mode : No Permission to Edit and Delete.
            </div>
            @endif
                <h4 class="card-title">@lang('user.index.title')</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        <!-- <li><a href="{{ route('admin.transporters.create') }}" class="btn btn-primary add-btn btn-darken-3">Add Delivery People</a></li> -->
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard table-responsive">
                    <table class="table table-striped table-bordered file-export">
                        <thead>
                            <tr>
                                <th>@lang('user.index.sl_no')</th>
                                <th>@lang('user.index.name')</th>
                                <th>@lang('user.index.email')</th>
                                <th>@lang('user.index.image')</th>
                                <th>@lang('user.index.contact_details')</th>
                                <th>@lang('user.index.rating')</th>
                                <th>@lang('user.index.top-up')</th>
                                <th>@lang('user.index.action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($Users as $key=>$User)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$User->name}}</td>
                                    <td>
                                    
                                    {{substr($User->email, 0, 1).'****'.substr($User->email, strpos($User->email, "@"))}}
                                    
                                    </td>
                                    <td>
                                        @if($User->avatar) 
                                            <div class="bg-img com-img" style="background-image: url({{ asset($User->avatar) }});"></div>
                                        @else
                                            No Image
                                        @endif
                                    </td>
                                    <td>
                                    {{substr($User->phone, 0, 5).'****'}}
                                    
                                    </td>
                                    <td class="star">
                                        <input type="hidden" class="rating" readonly value="3"/>
                                    </td>
                                    <td>
                                        <button class="table-btn btn btn-icon btn-danger addamount" data-id="{{$User->id}}">@lang('user.index.top-up')
                                        </button>
                                    </td>
                                    <td>
                                        @if(Setting::get('DEMO_MODE')==1)
                                        <a href="{{ route('admin.users.edit', $User->id) }}" class="table-btn btn btn-icon btn-success"><i class="fa fa-pencil-square-o"></i></a>
                                        <button   class="table-btn btn btn-icon btn-danger" form="resource-delete-{{ $User->id }}" onclick="return confirm('Are you sure you want to delete this user?');"><i class="fa fa-trash-o"></i></button>
                                        @endif
                                        <form id="resource-delete-{{ $User->id }}" action="{{ route('admin.users.destroy', $User->id)}}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr><td colspan="50">@lang('user.index.no_record_found')</td></tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Add Amount Modal Starts -->
<div class="card"> 
    <div class="modal fade text-xs-left" id="add-amount">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="modal-title" id="myModalLabel1">Add Amount</h2>
                    <!-- <div><p id="order_timer"></p></div> -->
                </div>
                <div class="modal-body">
                    <div class="row m-0">
                        <dl class="order-modal-top">
                            <form class="">
                                <div class="row m-0">
                                    <input type="hidden" class="form-control col-md-5" name="user_id" id="user_id" value="">
                                </div>
                                <br/>
                                <div class="row m-0">
                                    <input type="text" class="form-control col-md-5 price" id="amount" name="amount" placeholder="@lang('user.create.add_money_in_wallet')" value="" autocomplete="off">
                                </div>
                                <div class="col-md-12 exist-msg" style="display: none;">
                                    <span class="help-block">
                                        <strong id="err-msg"></strong>
                                    </span>
                                </div>
                                <br/><br/>
                                <div class="row m-0">
                                    <button type="button" class="btn btn-accent addmoney">@lang('user.create.add_money_in_wallet')</button>
                                    <button type="reset" class="btn btn-danger cancel" data-dismiss="modal">@lang('user.create.cancel')</button>
                                </div>

                            </form>
                        </dl>
                    </div>
                </div>
                <!-- <div class="modal-footer">
                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                </div> -->
            </div>
        </div>
    </div>
</div>
<!-- Order List Modal Ends -->
@endsection
@section('scripts')
<script>
    $(document).on("click", ".addamount",function(){ 
        var user_id = $(this).data('id');
        $('#add-amount').modal('show');
        $('#user_id').val(user_id);
    });
    $(document).on("click", ".addmoney",function(){ 

        var user_id = document.getElementById("user_id").value;
        var amount = document.getElementById("amount").value;

        $.ajax({
            type:"POST",
            url:"{{url('admin/users/addamount')}}",
            data:{'user_id':user_id , 'amount' : amount,'_token': '{{ csrf_token() }}'},
            success : function(results) {
                //console.log(results);
                if(results){
                    $('.exist-msg').hide();
                    $('#user_id').val('');
                    $('#amount').val('');
                    $('#add-amount').modal('hide');

                }else{
                    $('.exist-msg').show();
                    $("#err-msg").text(results.message); 
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr);
                var errors = xhr.responseJSON.errors;
                $('.exist-msg').show();
                $.each( errors , function( key, value ) { 
                    $("#err-msg").text(value);
                }); 
            }
        });
    });
</script>
@endsection