<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopType extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'category',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
    ];

    /**
     * Category 
     */
    public function categories()
    {
        return $this->hasMany('App\Category','shop_type_id');
    }

    /**
     * ShopTypeCommision
     */
    public function commisionshoptype()
    {
        return $this->hasMany('App\ShopTypeCommision','shop_type_id');
    }
}
