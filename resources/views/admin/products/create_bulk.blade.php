@extends('admin.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">@lang('inventory.product.add_title')</h3>
    </div>
    <div class="card-body collapse in">
        <div class="card-block">

            <form role="form" method="POST" action="{{ route('admin.product.store_bulk') }}" enctype="multipart/form-data">
                {{ csrf_field() }}

                 <input type="hidden" name="shop" value="{{Request::get('shop')}}" />
                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('product_file') ? ' has-error' : '' }}">
                            <label for="product_file">Product Upload</label>

                            <input type="file" accept="file/*" required name="product_file" class="dropify form-control" id="product_file"  aria-describedby="fileHelp">


                            @if ($errors->has('product_file'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('product_file') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 mb-2">
                    <a href="{{ route('admin.products.index') }}" class="btn btn-warning mr-1">
                        <i class="ft-x"></i> Cancel
                    </a>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-check-square-o"></i> Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/admin/plugins/multiselect/css/multi-select.css') }}">
<link rel="stylesheet" href="{{ asset('assets/admin/plugins/dropify/dist/css/dropify.min.css') }}">
@endsection

@section('scripts')

@endsection