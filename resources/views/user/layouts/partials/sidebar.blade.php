
		<!-- Nav pills -->
			<ul class="nav nav-pills justify-content-center py-4 mb-5">

				<li @if(Request::segment(1)=='orders') class="nav-item"  @endif>
					<a class="nav-link active" href="{{url('/orders')}}"> Orders</a>
				</li>

				<li @if(Request::segment(1)=='offers') class="nav-item"  @endif>
					<a class="nav-link" href="{{url('/offers')}}">Offers</a>
				</li>

				<li @if(Request::segment(1)=='favourite') class="nav-item"  @endif>
					<a class="nav-link" href="{{url('/favourite')}}"> Favourites</a>
				</li>
			  
				<li @if(Request::segment(1)=='payments') class="nav-item"  @endif>
					<a class="nav-link" href="{{url('/payments')}}"> Payments</a>
				</li>
				<li @if(Request::segment(1)=='useraddress') class="nav-item"  @endif>
					<a class="nav-link" href="{{url('/useraddress')}}"> Addresses</a>
				</li>

			 
			</ul>
    







<!-- <div class="profile-left col-md-3 col-sm-12 col-xs-12">
    <ul class="nav nav-tabs payment-tabs" role="tablist">
        <li @if(Request::segment(1)=='orders') class="active"  @endif>
            <a href="{{url('/orders')}}"><span><i class="mdi mdi-shopping"></i></span> Orders</a>
        </li>
        <li @if(Request::segment(1)=='offers') class="active"  @endif>
            <a href="{{url('/offers')}}"><span><i class="mdi mdi-percent"></i></span>Offers</a>
        </li>
        <li @if(Request::segment(1)=='favourite') class="active"  @endif>
            <a href="{{url('/favourite')}}"><span><i class="mdi mdi-heart"></i></span> Favourites</a>
        </li>
        <li @if(Request::segment(1)=='payments') class="active"  @endif>
            <a href="{{url('/payments')}}"><span><i class="mdi mdi-credit-card"></i></span> Payments</a>
        </li>
        <li @if(Request::segment(1)=='useraddress') class="active"  @endif>
            <a href="{{url('/useraddress')}}"><span><i class="mdi mdi-map-marker-outline"></i></span> Addresses</a>
        </li>
    </ul>
</div> -->