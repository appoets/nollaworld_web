<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Shop;
use DB;

class ShopsTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shops')->truncate();
        DB::table('shops')->insert([
            [
                'name' => 'Walmart',
                'email' => 'walmart@demo.com',
                'password' => bcrypt('123456'),
                'address' => 'New Jersey,USA',
                'maps_address' => 'New Jersey,USA',
                'latitude' => '13.05871070',
                'longitude' => '80.27570630',
                'estimated_delivery_time' => '30',
                'phone' => '8765654345',
                'description' => 'Walmart Inc. is an American multinational retail corporation that operates a chain of hypermarkets, discount department stores, and grocery stores. Headquartered in Bentonville, Arkansas, the company was founded by Sam Walton ',
                'avatar' => url('/seeddata/shops/walmart.jpg'),
                'default_banner' => url('/seeddata/shops/prov-6.png'),
                'status' => 'active'
            ],
            [
                'name' => 'Kroger',
                'email' => 'kroger@demo.com',
                'password' => bcrypt('123456'),
                'address' => 'USA',
                'maps_address' => 'USA',
                'latitude' => '13.060499',
                'longitude' => '80.254417',
                'estimated_delivery_time' => '30',
                'phone' => '8745645678',
                'description' => "The Kroger Co., or simply Kroger, is an American retailing company founded by Bernard Kroger in 1883 in Cincinnati, Ohio. It is the United States's largest supermarket chain by revenue, the second-largest general retailer and the seventeenth largest company in the United States.",
                'avatar' => url('/seeddata/shops/kroger.png'),
                'default_banner' => url('/seeddata/shops/Home-Page-Banner-001.jpg'),
                'status' => 'active'
            ],
            [
                'name' => 'HEB',
                'email' => 'heb@demo.com',
                'password' => bcrypt('123456'),
                'address' => 'USA',
                'maps_address' => 'USA',
                'latitude' => '13.0551',
                'longitude' => '80.2221',
                'estimated_delivery_time' => '30',
                'phone' => '9809354653',
                'description' => 'H-E-B is an American privately held supermarket chain based in San Antonio, Texas, with more than 350 stores throughout the U.S. state of Texas, as well as in northeast Mexico. The company also operates Central Market, an upscale organic and fine foods retailer.',
                'avatar' => url('/seeddata/shops/heb.jpg'),
                'default_banner' => url('/seeddata/shops/GC-090717-Fruit-Veggies-FB.jpg'),
                'status' => 'active'
            ],
            [
                'name' => 'Aldi',
                'email' => 'aldi@demo.com',
                'password' => bcrypt('123456'),
                'address' => 'USA',
                'maps_address' => 'USA',
                'latitude' => '13.0595',
                'longitude' => '80.2425',
                'estimated_delivery_time' => '30',
                'phone' => '7645637465',
                'description' => 'good shop',
                'avatar' => url('/seeddata/shops/aldi.png'),
                'default_banner' => url('/seeddata/shops/vh9oeys6_fruit-banner.png'),
                'status' => 'active'
            ],
            [
                'name' => 'Albertsons',
                'email' => 'albertsons@demo.com',
                'password' => bcrypt('123456'),
                'address' => 'USA',
                'maps_address' => 'USA',
                'latitude' => '13.0590',
                'longitude' => '80.2542',
                'estimated_delivery_time' => '30',
                'phone' => '8967584657',
                'description' => 'Albertsons Companies LLC is an American grocery company founded and headquartered in Boise, Idaho. It is privately owned and operated by investors, including Cerberus Capital Management.',
                'avatar' => url('/seeddata/shops/albertsons_logo.jpg'),
                'default_banner' => url('/seeddata/shops/mango offer.jpg'),
                'status' => 'active'
            ],
            
        ]);       
        
    }
}
