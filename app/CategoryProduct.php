<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryProduct extends Model
{

    protected $table = 'category_product';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id',
        'product_id',
        'subcategory_id'
    ];

    /**
     * Categories that the category belongs to.
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    /**
     * Categories that the category belongs to.
     */
    public function categorylist()
    {
        return $this->belongsTo('App\Category','category_id');
    }

    /**
     * SubCategories that the subcategory belongs to.
     */
    public function subcategories()
    {
        return $this->belongsToMany('App\SubCategory');
    }

    /**
     * Products belonging to the category
     */
    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    /**
     * Products belonging to the category
     */
    public function productslist()
    {
        return $this->belongsTo('App\Product','product_id');
    }
}
