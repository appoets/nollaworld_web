<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Product;
use DB;

class ShopsBannerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shop_banners')->truncate();
        DB::table('shop_banners')->insert([
            [
                'shop_id' => 1,
                'product_id' => 1,
                'url' => asset('/seeddata/shops/banner/Bannerb4.jpg'),
                'position' => 1,
                'status' => 'active'
            ],
            [
                'shop_id' => 2,
                'product_id' => 11,
                'url' => asset('/seeddata/shops/banner/GC-090717-Fruit-Veggies-FB.jpg'),
                'position' => 2,
                'status' => 'active'
            ],
            [
                'shop_id' => 3,
                'product_id' => 21,
                'url' => asset('/seeddata/shops/banner/Home-Page-Banner-001.jpg'),
                'position' => 3,
                'status' => 'active'
            ],
            [
                'shop_id' => 4,
                'product_id' => 31,
                'url' => asset('/seeddata/shops/banner/prov-6.png'),
                'position' => 4,
                'status' => 'active'
            ],
            [
                'shop_id' => 5,
                'product_id' => 41,
                'url' => asset('/seeddata/shops/banner/vh9oeys6_fruit-banner.png'),
                'position' => 5,
                'status' => 'active'
            ],
            [
                'shop_id' => 6,
                'product_id' => 51,
                'url' => asset('/seeddata/shops/banner/mango_offer.jpg'),
                'position' => 6,
                'status' => 'active'
            ]

            
        ]);        
    }
}
