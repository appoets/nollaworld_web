@extends('admin.layouts.app')

@section('content')

<!-- File export table -->
<div class="row file">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header">
                @if(Setting::get('DEMO_MODE')==0)
                    <div class="col-md-12" style="height:50px;color:red;">
                        ** Demo Mode : No Permission to Edit and Delete.
                    </div>
                @endif
                <h4 class="card-title">@lang('inventory.shoptypecommision.title')</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        <li><a href="{{ route('admin.shoptypecommision.create') }}?shop={{Request::get('shop')}}" class="btn btn-primary add-btn btn-darken-3">@lang('inventory.shoptypecommision.add_shoptypecommision')</a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard table-responsive">
                    <table class="table table-striped table-bordered file-export">
                        <thead>
                            <tr>
                                <th>@lang('inventory.shoptypecommision.sl_no')</th>
                                <th>@lang('inventory.shoptypecommision.shopname')</th>
                                <th>@lang('inventory.shoptypecommision.shoptypename')</th>
                                <th>@lang('inventory.shoptypecommision.commision')</th>
                                <th>@lang('inventory.shoptypecommision.action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($ShopTypeCommision as $key=>$Commision)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$Commision->shop->name}}</td>
                                    <td>{{$Commision->shoptype->name}}</td>
                                    <td>{{$Commision->commision}}</td>
                                    <td>
                                        @if(Setting::get('DEMO_MODE')==1)
                                            <a href="{{ route('admin.shoptypecommision.edit', $Commision->id) }}?shop={{Request::get('shop')}}" class="table-btn btn btn-icon btn-success"><i class="fa fa-pencil-square-o"></i></a>
                                                    
                                            <button  class="table-btn btn btn-icon btn-danger" form="resource-delete-{{ $Commision->id }}" ><i class="fa fa-trash-o" onclick="return confirm('Are you sure you want to delete this commision?');"></i></button> 
                                        @endif
                                        <form id="resource-delete-{{ $Commision->id }}" action="{{ route('admin.shoptypecommision.destroy', $Commision->id)}}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                        </form>
                                    </td>
                                </tr>
                            @empty
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <h4>@lang('inventory.shoptypecommision.no_record_found')</h4>
                                </div>
                            </div>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- File export table -->
@endsection
