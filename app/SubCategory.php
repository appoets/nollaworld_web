<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategory extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'category_id',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
    ];

    /**
     * Categories that the subcategory belongs to.
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    /**
     * Products belonging to the category
     */
    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    /**
     * Categories that the category belongs to.
     */
    public function subcategoryproducts()
    {
        return $this->hasMany('App\CategoryProduct');
    }
}
